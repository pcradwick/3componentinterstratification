package layerSF;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import layerSF.LayerComponent.LayerAtomData;
import main.Strat3Main;
import utilities.Logging;

public class AtomDialog extends JDialog {
	private JOptionPane optionPane;
	private AtomDialog ad;
	private LayerComponent thisLayer;
	
	private JLabel nameLabel = new JLabel("Atom Name:");
	JComboBox<String> atomNames = new JComboBox<String>();
	private JLabel numAtomLabel = new JLabel("No. Atoms in layer:");
	private JTextField numAtoms = new JTextField(10);
	private JLabel zLabel = new JLabel("Z coordinate (Angstroms):");
	private JTextField zCoord = new JTextField(10);
	private JLabel bFacLabel = new JLabel("B Factor:");
	private JTextField bFac = new JTextField("3.0", 10);
	
	private JButton close = new JButton("Close");

	int atomNumber = -1;
	
	public AtomDialog(Frame owner, LayerComponent lc, int an) {
		super(owner);

		ad = this;
		thisLayer = lc;
		atomNumber = an;
		
		setTitle("Atom No. "+an);
		
		JPanel contents = initialiseLayout();
		if (thisLayer.isFilePopulated()) {
			populateAtomFields();
		}

//		LayerAtomData lad = findAtom(atomNumber);
//		
//		LayerAtomData initialValues = thisLayer.new LayerAtomData();
//		initialValues.setName(lad.getName());
//		initialValues.setNumPerLayer(lad.getNumPerLayer());
//		initialValues.setzCoord(lad.getzCoord());
//		initialValues.setbFactor(lad.getbFactor());

		Object[] array = {contents};
		Object[] options = {close};
		
		optionPane = new JOptionPane(array,
							JOptionPane.QUESTION_MESSAGE,
							JOptionPane.PLAIN_MESSAGE,
							null,
							options,
							options[0]);
		
		close.addActionListener(new ButtonListener());

		setContentPane(optionPane);
		
		int posX = owner.getX() + 200;
		int posY = owner.getY() + 200;
		setLocation(new Point(posX, posY));
		setSize(new Dimension(400, 250));
		
		setModal(true);
		setVisible(true);		
		setResizable(true);

	}

	private JPanel initialiseLayout() {
		atomNames.addItemListener(new AtomComboBoxListener());
		loadComboData();
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout(0, 10));
		
		JPanel layerPanel = new JPanel();
		layerPanel.setLayout(new BoxLayout(layerPanel, BoxLayout.Y_AXIS));
		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
		
		JPanel namePane = new JPanel();
		namePane.setLayout(new FlowLayout());
		namePane.add(nameLabel);
		namePane.add(atomNames);
		textPanel.add(namePane);
		
		JPanel numAtomPane = new JPanel();
		numAtomPane.setLayout(new FlowLayout());
		numAtomPane.add(numAtomLabel);
		numAtomPane.add(numAtoms);
		textPanel.add(numAtomPane);
		
		JPanel zCoordPane = new JPanel();
		zCoordPane.setLayout(new FlowLayout());
		zCoordPane.add(zLabel);
		zCoordPane.add(zCoord);
		textPanel.add(zCoordPane);
		
		JPanel bFactorPane = new JPanel();
		bFactorPane.setLayout(new FlowLayout());
		bFactorPane.add(bFacLabel);
		bFactorPane.add(bFac);
		textPanel.add(bFactorPane);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(close);
		
		layerPanel.add(textPanel);
		layerPanel.add(buttonPanel);
		
		topPanel.add(layerPanel, BorderLayout.CENTER);
		
		return topPanel;
	}

	/*
	 * If a layer file has been read, the data will
	 * have populated thisLayer in LayerDialog
	 */
	private void populateAtomFields() {
		LayerAtomData lad = findAtom(atomNumber);
		atomNames.setSelectedItem(lad.getName());
		numAtoms.setText(lad.getNumPerLayer());
		zCoord.setText(lad.getzCoord());
		bFac.setText(lad.getbFactor());
	}
	
	private LayerAtomData findAtom(int findThis) {
		LayerAtomData foundAtom = null;

		for (Iterator<LayerAtomData> it = thisLayer.getComponentAtomData().iterator(); it.hasNext();) {
			LayerAtomData lad = (LayerAtomData) it.next();
			if (lad.getAtomNumber().equals(""+atomNumber)) {
				foundAtom = lad;
				break;
			} else {
				continue;
			}
		}
		
		return foundAtom;
	}
	
	private void loadComboData() {
		ArrayList<String> layerAtomTypes = thisLayer.getComponentAtomTypes();
		
		for (Iterator it = layerAtomTypes.iterator(); it.hasNext();) {
			String str = (String) it.next();
			atomNames.addItem(str);
		}
	}

	//////////////////////////////////////////////////////////////////
	//			INNER CLASSES
	//////////////////////////////////////////////////////////////////
	class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ev) {
				LayerComponent.LayerAtomData layerAtomData = null;
				if (thisLayer.isFilePopulated()) {
					layerAtomData = findAtom(atomNumber);
				} else {
					layerAtomData = thisLayer.new LayerAtomData();
				}
				
				layerAtomData.setAtomNumber(""+atomNumber);
				layerAtomData.setName((String) atomNames.getSelectedItem());
				layerAtomData.setNumPerLayer(numAtoms.getText());
				layerAtomData.setzCoord(zCoord.getText());
				layerAtomData.setbFactor(bFac.getText());

				if (!thisLayer.isFilePopulated()) {
					thisLayer.getComponentAtomData().add(layerAtomData);
				}
				
				Logging.writeToLog(true, "Layer Atom = "+layerAtomData.getName(), Logging.DEBUG);
			switch (ev.getActionCommand()) {
			case "Close":
				ad.dispose();
				break;

			}
		}
	}

	// Not used
	private final class AtomComboBoxListener implements ItemListener {
		// This is called when an item is changed or clicked - if more... re-populate ComboBox
		@Override
		public void itemStateChanged(ItemEvent itemEvent) {
			if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
				String itemSelected = (String) itemEvent.getItem();

//		        Logging.writeToLog(true, "Layer Atom = "+itemSelected, Logging.DEBUG);
			}
		}

	}
}
