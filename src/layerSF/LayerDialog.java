package layerSF;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import layerSF.LayerComponent.LayerAtomData;
import main.Strat3Main;
import utilities.Logging;
import utilities.SystemSettings;

public class LayerDialog extends JDialog {
	private Frame owner;
	private JOptionPane optionPane;
	private LayerDialog dd;
	private LayerComponent thisLayer;
	
	private JLabel atomLabel = new JLabel("Total Atoms in layer:");
	private JTextField atoms = new JTextField("", 10);
	private JLabel rStarLabel = new JLabel("Max r*:");
	private JTextField rStar = new JTextField("", 10);

	private JButton close = new JButton("Close");
	private JButton atomData = new JButton("Get Atom Data");

	String EOL = SystemSettings.instance().getLineTerminator();
	
	boolean filePopulated = false;
	
	
	public LayerDialog(Frame frame, LayerComponent lc) {
		super(frame);
		
		owner = frame;
		dd = this;
		thisLayer = lc;
		
		setTitle("Data for Layer "+thisLayer.getComponentNumber()+": "+thisLayer.getName());
		JPanel contents = initialiseLayout();

		Object[] array = {contents};
		Object[] options = {close, atomData};
		
		optionPane = new JOptionPane(array,
							JOptionPane.QUESTION_MESSAGE,
							JOptionPane.PLAIN_MESSAGE,
							null,
							options,
							options[0]);
		
		close.addActionListener(new ButtonListener());
		atomData.addActionListener(new ButtonListener());

		setContentPane(optionPane);
		
		int posX = owner.getX() + 100;
		int posY = owner.getY() + 100;
		setLocation(new Point(posX, posY));
		setSize(new Dimension(300, 200));
		
		setModal(true);
		setVisible(true);		
		setResizable(true);
	}

	
	private JPanel initialiseLayout() { 
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout(0, 10));
		
		JPanel layerPanel = new JPanel();
		layerPanel.setLayout(new BoxLayout(layerPanel, BoxLayout.Y_AXIS));
		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
		
		JPanel atomPane = new JPanel();
		atomPane.setLayout(new FlowLayout());
		atomPane.add(atomLabel);
		
		if (thisLayer.isFilePopulated()) {
			atoms.setText(thisLayer.getNumAtoms());
		}
		
		atomPane.add(atoms);
		textPanel.add(atomPane);
		
		JPanel rsPane = new JPanel();
		rsPane.setLayout(new FlowLayout());
		rsPane.add(rStarLabel);
		double rS = Strat3Main.getMainFrame().getMaxRStar();
		if (rS > 0.0) {
			rStar.setText(""+rS);
		}
		rsPane.add(rStar);
		textPanel.add(rsPane);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(close);
		buttonPanel.add(atomData);
		
		layerPanel.add(textPanel);
		layerPanel.add(buttonPanel);
		
		topPanel.add(layerPanel, BorderLayout.CENTER);
		
		return topPanel;
	}
	
	private void getAtomData() {
		int numAtoms = Integer.parseInt(thisLayer.getNumAtoms());
		
		for (int i=0; i < numAtoms; ++i) {
			new AtomDialog(owner, thisLayer, i+1);
		}
		
		saveLayer(thisLayer);
	}
	
	// Populate thisLayer from file data
	private void populateLayout(String name) {
//		String layerFileName = thisLayer.getFileName();
//		File layerFile = new File(layerFileName);

		if (thisLayer.isLayerFileExists()) {
			try {
				List<String> lines = Files.readAllLines(Paths.get(thisLayer.getFileName()));
				
				for(String line : lines) {
					String [] words = line.split(" ");
					switch (words[0]) {
					case "LayerNumber":
						continue;

					case "LayerName":
						thisLayer.setName(words[2]);
						break;
						
					case "LayerNumAtoms":
						thisLayer.setNumAtoms(words[2]);
						atoms.setText(words[2]);
						break;
					
					case "MaxRStar":
						thisLayer.setMaxRStar(words[2]);
						rStar.setText(words[2]);
						break;
						
					case "AtomTypes":
						for (int i=2; i< words.length; ++i) {
							thisLayer.getComponentAtomTypes().add(words[i]);
						}
						break;
					
					case "Atom":
						LayerAtomData atom = thisLayer.new LayerAtomData();
						atom.setAtomNumber(words[1]);
						atom.setName(words[2]);
						atom.setNumPerLayer(words[3]);
						atom.setzCoord(words[4]);
						atom.setbFactor(words[5]);
						thisLayer.getComponentAtomData().add(atom);
						break;
					
					default:
						Logging.writeToLog(true, "Unknown line: "+line+" in file: "+name, Logging.ERROR);
					}
					
				}
			} catch(Exception e) {
				String stackTrace = ExceptionUtils.getStackTrace(e);
				Logging.writeToLog(true, e.toString()+" reading file in LayerDialog.populateLayout. StackTrace:\n"+stackTrace, Logging.ERROR);
			}
				
			filePopulated = true;
		} else {
			Logging.writeToLog(false, "No layer file for "+name, Logging.INFO);
		}
		
		return;
	}
	
	private void saveLayer(LayerComponent layer) {
		if (layer.getName() == null || layer.getName().isEmpty()) {
			Logging.writeToLog(true, "No layer data to save", Logging.WARN);
			return;
		}
		
		String layerFileName = thisLayer.getFileName();

		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			File f = new File(layerFileName);
			if (f.exists()) {
				FileUtils.forceDelete(f);
			}
			File g = new File(layerFileName);
			
			fw = new FileWriter(g) ;
			bw = new BufferedWriter(fw);
			bw.write("LayerNumber = "+layer.getComponentNumber()+EOL);
			bw.write("LayerName = "+layer.getName()+EOL);
			bw.write("LayerNumAtoms = "+layer.getNumAtoms()+EOL);
			bw.write("MaxRStar = "+layer.getMaxRStar()+EOL);
			
			String types = "";
			for (Iterator<String> itt = layer.getComponentAtomTypes().iterator(); itt.hasNext();) {
				String aType = (String) itt.next();
				types += aType+" ";
			}
			
			bw.write("AtomTypes = "+types+EOL);
			
			for (Iterator<LayerAtomData> ita = layer.getComponentAtomData().iterator(); ita.hasNext();) {
				LayerAtomData lad = (LayerAtomData) ita.next();
				String aData = "Atom "+lad.getAtomNumber()+" "+lad.getName()+" "+
							lad.getNumPerLayer()+" "+lad.getzCoord()+" "+lad.getbFactor()+EOL;
				bw.write(aData);
			}
		} catch(Exception e) {
			String stackTrace = ExceptionUtils.getStackTrace(e);
			Logging.writeToLog(true, e.toString()+" writing file in LayerDialog.saveLayer. StackTrace:\n"+stackTrace, Logging.ERROR);
		} finally {
			try {
				bw.close();
				fw.close();
			} catch(Exception e) {
				String stackTrace = ExceptionUtils.getStackTrace(e);
				Logging.writeToLog(true, e.toString()+" closing in LayerDialog.saveLayer. StackTrace:\n"+stackTrace, Logging.ERROR);
			}
		}
		
		
		
	}
	//////////////////////////////////////////////////////////////////
	//			INNER CLASSES
	//////////////////////////////////////////////////////////////////
	class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ev) {
			if (!filePopulated) {
//				thisLayer.setName(title.getText());
				thisLayer.setNumAtoms(atoms.getText());
				thisLayer.setMaxRStar(rStar.getText());
			
				double rS = Double.parseDouble(rStar.getText());
				// Round sum to 2 decimal places
				DecimalFormat df2 = new DecimalFormat("####.##");
				double round2 = Double.valueOf(df2.format(rS));
				Strat3Main.getMainFrame().setMaxRStar(round2);
			}
			
			switch (ev.getActionCommand()) {
			case "Close":
				saveLayer(thisLayer);
				dd.dispose();
				break;
				
			case "Get Atom Data":
				if (atoms.getText().isEmpty() || rStar.getText().isEmpty()) {
					JOptionPane.showMessageDialog(dd, "All fields must be populated when data not read from file");
				} else {
					dd.getAtomData();				
				}
				break;
				
			}
		}
	}
	
//	class TitleListener implements FocusListener {
//
//		@Override
//		public void focusGained(FocusEvent e) {
//			// TODO Auto-generated method stub
//			
//		}
//
//		@Override
//		public void focusLost(FocusEvent e) {
//			populateLayout(title.getText());
//		}
//		
//	}
	

	
	//////////////////////////////////////////////////////////////
	
}
