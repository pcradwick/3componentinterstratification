package layerSF;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import formfactors.RStarAndF;
import formfactors.FormFactorCurves.CurvePlotter;
import layerSF.LayerComponent.LayerAtomData;
import main.Strat3Main;
import utilities.Logging;

/**	Use Dialogs (LayerDialog & AtomDialog) to get layer data and atom parameters for each layer
 * Component data:
 * 		Title
 * 		Number of atoms (i.e. the no. of Atom parameter records - NA, NB, NC)
 * 
 * Atom parameters:
 * 		Name
 * 		Number per component
 * 		z coordinate
 * 		Bfactor			(Thermal motion coefficient - BFAC)
 * 
 * @author peter
 *
 */

public class CalculateLayerSF {

	private int numAtomTypes = -1;
	private int numComponents = Strat3Main.getMainFrame().getNumComponents();
	private String maxRStar = "";
	
	LayerComponent thisLayer;
	Map<String, Double> sfCurves;
	LayerComponent [] components = {null, null, null};
	ArrayList<OneRStarSums> sfSums = null;
	
	boolean displayCurves;
	
	public CalculateLayerSF(boolean display) {
		displayCurves = display;
		
		sfSums = Strat3Main.getMainFrame().getSfSums();
		
		Logging.writeToLog(false, "Calculating Layer Structure Factors for "+numComponents+" components", Logging.DEBUG);
		
		for (int i=0; i < numComponents; ++i) {
			components[i] = Strat3Main.getMainFrame().getLayers().get(i);
		}
		
		numAtomTypes = Strat3Main.getMainFrame().getUniqueAtoms().size();
		calculateLayerSFSums();
		Strat3Main.getMainFrame().setLayerStructureFactorSums(sfSums);
	}
	
	/*
	 * Calculate Layer Structure Factor curves from r* = 0 -> 0.75 (MaxRStar)
	 * in intervals of 0.01
	 */
	private void calculateLayerSFSums() {
		long npts = (long) (Strat3Main.getMainFrame().getMaxRStar()* 100 + 1);
		double rStar, twoPiRStar, sinThOL;	// sinThOL = rStar/2
		int [] layK = {0, 1, 2, 0, 0, 1, 1, 2, 2}, layL = {0, 1, 2, 1, 2, 0, 2, 0, 1};

		for (int np=0; np<npts; ++np) {		// loop over no. of points
			rStar = (double) np * 0.01;
			twoPiRStar = rStar * 2 * Math.PI;
			sinThOL = rStar / 2;
			sfCurves = interpolateScatteringFactorTables(rStar);
			if (sfCurves.size() != numAtomTypes) {
				Logging.writeToLog(true, "Error interpolating scattering factor curves. Unable to proceed", Logging.ERROR);
				return;
			}

			/*
			 * Consider all different combinations of the 3 layers in pairs
			 * i.e. AA, BB, CC, AB, AC, BA, BC, CA, CB
			 * nc = 0   1   2	3   4   5	6	7	8
			 * layK 0   1   2   0   0	1	1	2	2
			 * layL 0   1   2   1	2	0	2	0	1
			 * 
			 * These loops calculate the layer structure factor terms
			 * 		V(kk) and V(kl) 	ClayMinerals (1975), 10, 347
			 * 		V(A)**2, V(B)**2, V(C)**2, C(AB), S(AB), C(AC), S(AC),
			 * 		C(AC), S(AC)		Clay Minerals (1978), 13, 53
			 * 
			 * The resulting sums are stored in Strat3GUI ArrayList<OneRStarSums sfSums
			 * 		sumCosAA[0], 	sumCosBB[1],	sumCosCC[2], 
			 * 		sumCosAB[3],	sumSinAB[4],	sumCosAC[5],	sumSinAC[6], 
			 * 		sumCosBA[7],	sumSinBA[8],	sumCosBC[9],	sumSinBC[10],
			 * 		sumCosCA[11],	sumSinCA[12],	sumCosCB[13],	sumSinCB[14]
			 * 
			 */

			double sfK, sfL, BFacK, BFacL, bCorrK, bCorrL, zK, zL;
			double Arg, SumC, SumS;
			String atomTypeK, atomTypeL;

			OneRStarSums rsSums = new OneRStarSums(rStar);
			
			for (int nc=0; nc<9; ++nc) {
				if (numComponents == 1 && (nc > 0)) {
					continue;		// Exclude combinations involving layers B & C
				}
				
				if (numComponents == 2 && (nc == 2 || nc == 4 || nc > 5)) {
					continue;		// Exclude combinations involving layer C
				}
				
				
				int numLayerK = Integer.parseInt(components[layK[nc]].getNumAtoms());
				int numLayerL = Integer.parseInt(components[layL[nc]].getNumAtoms());

				SumC = SumS = 0.0;
				for (int kk=0; kk<numLayerK; ++kk) {
					LayerAtomData atomK = components[layK[nc]].getComponentAtomData().get(kk);
					atomTypeK = atomK.getName();
					zK = Double.parseDouble(atomK.getzCoord());
					BFacK = Double.parseDouble(atomK.getbFactor());
					bCorrK = Math.exp(-BFacK*(sinThOL*sinThOL));
					sfK = Double.parseDouble(atomK.getNumPerLayer()) * sfCurves.get(atomTypeK)* bCorrK;

					for (int ll=0; ll<numLayerL; ++ll) {
						LayerAtomData atomL = components[layL[nc]].getComponentAtomData().get(ll);
						atomTypeL = atomL.getName();
						zL = Double.parseDouble(atomL.getzCoord());
						BFacL = Double.parseDouble(atomL.getbFactor());
						bCorrL = Math.exp(-BFacL*(sinThOL*sinThOL));
						sfL = Double.parseDouble(atomL.getNumPerLayer()) * sfCurves.get(atomTypeL)* bCorrL;

						Arg = twoPiRStar * (zK - zL);

						SumC += sfK * sfL * Math.cos(Arg);
						SumS += sfK * sfL * Math.sin(Arg);		// Arg = 0 for nc = 0, 1, 2
					}
				}			// End of summation atom loops

				switch(nc) { 
				case 0:
					rsSums.storeCosAA(SumC);			// SumS = 0
					break;

				case 1:
					rsSums.storeCosBB(SumC);			// SumS = 0
					break;

				case 2:
					rsSums.storeCosCC(SumC);			// SumS = 0
					break;

				case 3:
					rsSums.storeCosAB(SumC);
					rsSums.storeSinAB(SumS);
					break;

				case 4:
					rsSums.storeCosAC(SumC);
					rsSums.storeSinAC(SumS);
					break;

				case 5:
					rsSums.storeCosBA(SumC);
					rsSums.storeSinBA(SumS);
					break;
					
				case 6:
					rsSums.storeCosBC(SumC);
					rsSums.storeSinBC(SumS);
					break;
					
				case 7:
					rsSums.storeCosCA(SumC);
					rsSums.storeSinCA(SumS);
					break;
					
				case 8:
					rsSums.storeCosCB(SumC);
					rsSums.storeSinCB(SumS);
					break;
					
				}
				
				sfSums.add(rsSums);
			}			// End of combination loop nc
			
		}				// End of no. r* points loop
		
		if (displayCurves) {
			plotCurves();
		}
	}
	/*
	 *  The return value is a Map containing the interpolated f value at the point
	 *  rStar for all atom types. The String is the atom name, Double is the f value.
	 */
	private Map<String, Double> interpolateScatteringFactorTables(double rStar) {
		sfCurves = new HashMap<String, Double>();
		Object [] key = Strat3Main.getMainFrame().getUniqueAtoms().toArray();
		
		atoms:for (int c=0; c<numAtomTypes; ++c) {	// Loop over atom curves
			ArrayList<RStarAndF> aCurve = Strat3Main.getMainFrame().getfCurves().get((String) key[c]);
			RStarAndF lastRsf = null;
			double deltaRStar = 0, fact = 0, intF = 0;
			boolean found = false;
			
			fcurve:for (Iterator<RStarAndF> it = aCurve.iterator(); it.hasNext();) {
				RStarAndF rsf = (RStarAndF) it.next();
				if (rsf.getrStar() <= rStar) {
					lastRsf = rsf;
					continue fcurve;
				} else {
					found = true;
					deltaRStar = rsf.getrStar() - lastRsf.getrStar();
					fact = (rStar - lastRsf.getrStar()) / deltaRStar;
					intF = rsf.getF()*fact + lastRsf.getF()*(1 - fact);
					sfCurves.put((String) key[c], intF);
					break fcurve;
				}
			}
			
			if (!found) {
				String msg = "The r* requested for interpolation "+rStar+" is greater than maxR* "+maxRStar;
				Logging.writeToLog(true, msg, Logging.ERROR);
				break atoms;
			}

		}			// End of atom loop

		return sfCurves;
	}
	
	public void plotCurves() {
		String [] sumTypes = {"AA", "BB", "CC", "cosAB", "sinAB", "cosAC", "sinAC", "cosBA", "sinBA", "cosBC", "sinBC", "cosCA", "sinCA", "cosCB", "sinCB"};
		ArrayList<OneRStarSums> sfSums = Strat3Main.getMainFrame().getSfSums();

		if (numComponents == 1) {
			XYSeries series = new XYSeries("SF");
			series = new XYSeries("SF");
			for (Iterator<OneRStarSums> it = sfSums.iterator(); it.hasNext();) {
				OneRStarSums orss = (OneRStarSums) it.next();
				series.add(orss.getrStar(), orss.getSums()[0]);
			}

			CurvePlotter cp = new CurvePlotter("Layer Structure Factor Curves", series, sumTypes[0]);
			cp.pack();
			cp.setVisible(true);
		} else if (numComponents == 2) {
			XYSeries [] series = new XYSeries[15];
			for (int i=0; i<15; ++i) {
				if (numComponents == 2 && (i == 2 || i == 5 || i == 6 || i > 8)) {
					continue;		// Exclude combinations involving layer C
				}

				series[i] = new XYSeries("SF");
				for (Iterator<OneRStarSums> it = sfSums.iterator(); it.hasNext();) {
					OneRStarSums orss = (OneRStarSums) it.next();
					series[i].add(orss.getrStar(), orss.getSums()[i]);
				}

				CurvePlotter cp = new CurvePlotter("Layer Structure Factor Curves", series[i], sumTypes[i]);
				cp.pack();
				cp.setVisible(true);
			}

		} else {
			XYSeries [] series = new XYSeries[15];
			for (int i=0; i<15; ++i) {
				if (numComponents == 2 && (i == 2 || i > 4)) {
					continue;		// Exclude combinations involving layer C
				}

				series[i] = new XYSeries("SF");
				for (Iterator<OneRStarSums> it = sfSums.iterator(); it.hasNext();) {
					OneRStarSums orss = (OneRStarSums) it.next();
					series[i].add(orss.getrStar(), orss.getSums()[i]);
				}

				CurvePlotter cp = new CurvePlotter("Layer Structure Factor Curves", series[i], sumTypes[i]);
				cp.pack();
				cp.setVisible(true);
			}
		}

	}


	//////////////////////////////////////////////////////////////////////
	//					INNER CLASSES
	//////////////////////////////////////////////////////////////////////

	public class CurvePlotter extends ApplicationFrame {
		
		public CurvePlotter(String title, XYSeries series, String sumType) {
			super(title);
			
			XYSeriesCollection data = new XYSeriesCollection(series);
			JFreeChart chart = ChartFactory.createXYLineChart(
					"Layer Structure Factor curve for "+sumType,
					"r*",
					"Phi",
					data,
					PlotOrientation.VERTICAL,
					false,
					false,
					false
					);
			
			ChartPanel chartPanel = new ChartPanel(chart);
			chartPanel.setPreferredSize(new Dimension(500, 200)); 
			setContentPane(chartPanel);
		}
		
	}
}
