package layerSF;

public class OneRStarSums {
	double [] sums = new double[15];
	double rStar = 0;
	/*
	 * 	Storage for the end layer sums for one r* value:
	 * 		sumCosAA[0],	sumCosBB[1],	sumCosCC[2],
	 * 		sumCosAB[3],	sumSinAB[4],	sumCosBA[5],	sumSinBA[6],
	 * 		sumCosAC[7],	sumSinAC[8],	sumCosCA[9],	sumSinCA[10], 
	 * 		sumCosBC[11],	sumSinBC[12],	sumCosCB[13],	sumSinCB[14]
	 */
	
	public OneRStarSums(double rS) {
		rStar = rS;
	}

	public void storeCosAA(double sumCosAA) {
		sums[0] = sumCosAA;
	}
	
	public void storeCosBB(double sumCosBB) {
		sums[1] = sumCosBB;
	}
	
	public void storeCosCC(double sumCosCC) {
		sums[2] = sumCosCC;
	}
	
	public void storeCosAB(double sumCosAB) {
		sums[3] = sumCosAB;
	}
	
	public void storeSinAB(double sumSinAB) {
		sums[4] = sumSinAB;
	}
	
	public void storeCosBA(double sumCosBA) {
		sums[5] = sumCosBA;
	}
	
	public void storeSinBA(double sumSinBA) {
		sums[6] = sumSinBA;
	}
	
	public void storeCosAC(double sumCosAC) {
		sums[7] = sumCosAC;
	}
	
	public void storeSinAC(double sumSinAC) {
		sums[8] = sumSinAC;
	}
	
	public void storeCosCA(double sumCosCA) {
		sums[9] = sumCosCA;
	}
	
	public void storeSinCA(double sumSinCA) {
		sums[10] = sumSinCA;
	}
	
	public void storeCosBC(double sumCosBC) {
		sums[11] = sumCosBC;
	}
	
	public void storeSinBC(double sumSinBC) {
		sums[12] = sumSinBC;
	}

	public void storeCosCB(double sumCosCB) {
		sums[13] = sumCosCB;
	}
	
	public void storeSinCB(double sumSinCB) {
		sums[14] = sumSinCB;
	}

	public double[] getSums() {
		return sums;
	}

	public double getrStar() {
		return rStar;
	}
	

}
