package layerSF;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import utilities.Logging;
import utilities.SystemSettings;

/** storage for all data pertaining to a single Component i.e.
 * 		Name of Component
 * 		Total number of atoms in the Component (NAL)
 * 		An array of different atom types in this Component
 * 		An array of NAL values each containing the
 * 			Atom parameters for each atom
 * 				Name, 
 * 				Number of this type of atom in Component
 * 				relative Z coordinate in Angstroms. Lowest atom has z = 0
 * 				Thermal factor correction for f curves (B Factor)
 * 
 * @author peter
 *
 */

public class LayerComponent {
	private int componentNumber = -1;
	private String name = "";
	private String numAtoms = "";
	private String maxRStar = "";
	private ArrayList<String> componentAtomTypes;
	private ArrayList<LayerAtomData> componentAtomData;

	private boolean layerFileExists = false;
	private boolean filePopulated = false;
	private String fileName = null;
	
	String slash = SystemSettings.instance().getFileSeparator();
	String layoutDir = SystemSettings.instance().getWorkingDir()+slash+"Layer Data";

	public LayerComponent(int num) {
		componentNumber = num;
		componentAtomData = new ArrayList<LayerAtomData>();
		componentAtomTypes = new ArrayList<String>();
		
		// Create directory for Layer files
		File fileDir = new File(layoutDir);
		try {
			if (!fileDir.exists()) {
				FileUtils.forceMkdir(fileDir);
			}
		} catch(Exception e) {
			String stackTrace = ExceptionUtils.getStackTrace(e);
			Logging.writeToLog(true, e.toString()+" creating dir in LayerComponent. StackTrace:\n"+stackTrace, Logging.ERROR);
		}
	}

	
	
	///////////////////////////////////////////////////////////////////////
	//					INNER CLASSES
	///////////////////////////////////////////////////////////////////////

	public class LayerAtomData {
		private String atomNumber = "";
		private String name = "";
		private String numPerLayer = "";
		private String zCoord = "";
		private String bFactor = "";
		
		public LayerAtomData() {}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getNumPerLayer() {
			return numPerLayer;
		}

		public void setNumPerLayer(String numPerLayer) {
			this.numPerLayer = numPerLayer;
		}

		public String getzCoord() {
			return zCoord;
		}

		public void setzCoord(String zCoord) {
			this.zCoord = zCoord;
		}

		public String getbFactor() {
			return bFactor;
		}

		public void setbFactor(String bFactor) {
			this.bFactor = bFactor;
		}

		public String getAtomNumber() {
			return atomNumber;
		}

		public void setAtomNumber(String an) {
			this.atomNumber = an;
		}
	}

	/////////////////////////////////////////////////////////////
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumAtoms() {
		return numAtoms;
	}

	public void setNumAtoms(String numAtoms) {
		this.numAtoms = numAtoms;
	}

	public ArrayList<LayerAtomData> getComponentAtomData() {
		return componentAtomData;
	}

	public void setComponentAtomData(ArrayList<LayerAtomData> layerAtomData) {
		this.componentAtomData = layerAtomData;
	}

	public int getComponentNumber() {
		return componentNumber;
	}

	public ArrayList<String> getComponentAtomTypes() {
		return componentAtomTypes;
	}

	public void setComponentAtomTypes(ArrayList<String> layerAtomTypes) {
		this.componentAtomTypes = layerAtomTypes;
	}

	public String getMaxRStar() {
		return maxRStar;
	}

	public void setMaxRStar(String maxRStar) {
		this.maxRStar = maxRStar;
	}

	public boolean isLayerFileExists() {
		return layerFileExists;
	}

	public void setLayerFileExists(boolean layerFileExists) {
		this.layerFileExists = layerFileExists;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getLayoutDir() {
		return layoutDir;
	}

	public boolean isFilePopulated() {
		return filePopulated;
	}

	public void setFilePopulated(boolean filePopulated) {
		this.filePopulated = filePopulated;
	}

	///////////////////////////////////////////////////////////////////


}
