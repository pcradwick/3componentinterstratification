package utilities;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.appender.RollingFileAppender;

public class Logging {
	// Level hierarchy: DEBUG, INFO, WARN, ERROR, FATAL
	public static final int DEBUG		= 0;
	public static final int INFO		= 1;
	public static final int WARN		= 2;
	public static final int ERROR		= 3;
	public static final int FATAL		= 4;
	public static final int SERVER		= 5;
	public static final int CLIENT		= 6;

	/*
	 *  The log files to send on activation of the bug report button
	 *  To change the logs sent, change/add the constants below and
	 *  update the arrays clientLogsRequired and /or serverLogsRequired ass appropriate
	 */
	
	private static String WORKING_DIR			= null;
	private static String CURRENT_CLIENT_LOG	= null;
	private static String CURRENT_SERVER_LOG	= null;
	private static String CURRENT_ACTIVITY_LOG	= null;
	private static String CURRENT_CLIENT_DIR	= null;
	private static String CURRENT_SERVER_DIR	= null;
	public static String LOG_ARCHIVE_DIR		= null;				// Dynamic see resetArchiveDir()
	
	public static int NUM_ARCHIVE_LOGS			= 5;				// Each for Server & Client Logs
	public static int KEEP_LOGS_FOR_MONTHS		= 3;				// In LogArchive dir
	private static String [] currentServerLogs	= new String[2];	// Server Log & Activity Log names
	
	private static String errLogEmailRecipient = null;	// errorLogs@highburyeye.co.nz

	public static int runningApplication = -1;
	public static Level serverLogMode = Level.DEBUG;
	public static Level clientLogMode = Level.DEBUG;

	/*
	 * These names must match those in the Log4j2.xml configuration file
	 */
	public static final String CLIENT_LOGGER_NAME = "clientLogger";
	public static final String SERVER_LOGGER_NAME = "serverLogger";
	public static final String ACTIVITY_LOGGER_NAME = "activityLogger";
	
	public static final String CLIENT_APPENDER = "clientAppender";
	public static final String SERVER_APPENDER = "serverAppender";
	public static final String ACTIVITY_APPENDER = "activityAppender";
	public static final String ARCHIVE_DIR = "LogArchive";
	
	private String TEMP_LOG_DIR_NAME = "CheckedLogs";
	private String TEMP_FILE_NAME = "checkedLines.log";
	
    private static org.apache.logging.log4j.Logger currentLogger = null;

    private static Logging instance;
    
    /*
     * Use this switch for testing.
     * If true, write the actions, like sending reminders,
     * to a log file instead of sending to a patient
     */
    
	public static final boolean LOG_ACTIONS = true;
  
	// Return the single instance of ClientMain
	public synchronized static Logging instance(int thisApp) {
		if (instance == null){
			instance = new Logging(thisApp);
		}
		
		resetArchiveDir();
		return instance;
	}

	private Logging(int thisApp) {

		runningApplication = thisApp;
		
		/*
		 * ALL Appenders and loggers are defined in the configuration file.
		 *
		 * The RootLogs are intended for I logging - not used
		 * The Activity and Server Logs are stored on the Server
		 * The Client Logs are stored on the Client
		 * 
		 * Set up log4j2 for 3 independent log files: Client log, Server Log and Server activity
		 * The Server activity uses filters to select relevant data when message button is pressed.
			.../i-base/ClientLogs/ClientLog.log
			.../i-base/ServerLogs/ServerLog.log
			.../i-base/ServerLogs/ActivityLog.log
			
			Archived logs are in directories of the general form:
			logDir/ServerLog.n.gz, logDir/ClientLog.n.gz etc...
			where logDir = .../i-base/LogArchive/yyyy-MM/yyyy-MM-dd
			and where .../i-base is where the app is run from, i.e. location of jar file
			This is defined in workingDir
			The latest logs have the highest number, n
		 */

		// Get the name of the logs from the configuration file
		Logger clientLogger = LogManager.getLogger(CLIENT_LOGGER_NAME);
		org.apache.logging.log4j.core.Logger cCorelogger = (org.apache.logging.log4j.core.Logger) clientLogger;
		Appender cltAppender = cCorelogger.getAppenders().get(CLIENT_APPENDER);
		String clientLogPath = ((RollingFileAppender) cltAppender).getFileName(); // Includes DIR

//		Logger serverLogger = LogManager.getLogger(SERVER_LOGGER_NAME);
//		org.apache.logging.log4j.core.Logger sCorelogger = (org.apache.logging.log4j.core.Logger) serverLogger;
//		Appender svrAppender = sCorelogger.getAppenders().get(SERVER_APPENDER);
//		String serverLogPath = ((RollingFileAppender) svrAppender).getFileName(); // Includes DIR
//
//		Logger activityLogger = LogManager.getLogger(ACTIVITY_LOGGER_NAME);
//		org.apache.logging.log4j.core.Logger aCorelogger = (org.apache.logging.log4j.core.Logger) activityLogger;
//		Appender actAppender = aCorelogger.getAppenders().get(ACTIVITY_APPENDER);
//		String activityLogPath = ((RollingFileAppender) actAppender).getFileName(); // Includes DIR

		// NOTE: Log4j uses separator = / on both Linux & Windows
		// Client:
		int slashPos = clientLogPath.indexOf("/");
		String nameOnly = clientLogPath.substring(slashPos+1, clientLogPath.length());
		CURRENT_CLIENT_LOG = nameOnly;
		String clientLogDir  = clientLogPath.substring(0,slashPos);

		// Create paths (only) for current logs
		WORKING_DIR = SystemSettings.instance().getWorkingDir();
		CURRENT_CLIENT_DIR = WORKING_DIR+SystemSettings.instance().getFileSeparator()+clientLogDir+SystemSettings.instance().getFileSeparator();
		
		String app = (runningApplication == CLIENT)?"Client":"Server";
		Logging.writeToLog(false, "Initialising Logging for "+app , Logging.DEBUG);
	}
	
	private static void resetArchiveDir() {				
		/*
		 *  Log Archive directories for the last 2 days are stored
		 *  in case the todays doesn't have NUM_ARCHIVE_LOGS logs
		 *  LOG_ARCHIVE_DIR depends on today's date
		 */
		CurrentDateTime cdt = new CurrentDateTime();
		String today = cdt.getDate();						//	yyyy-MM-dd
		String month = today.substring(0, 7);				//	yyyy-MM
		
		String app = (runningApplication == CLIENT)?"Client":"Server";
		Logging.writeToLog(false, "Resetting Archive Dir for "+app , Logging.DEBUG);
		
		////////////////////////////////////////////////////////////////////////////////
		//	Using Java 8 API
//		LocalDateTime dateTime = LocalDateTime.now();
//		LocalDate date = dateTime.toLocalDate();
// 		int mnth = dateTime.getMonth().getValue();
//        int yr = dateTime.getYear();
//        int day = dateTime.getDayOfMonth();
//        String today1 = yr+"-"+mnth+"-"+day;
//        String month1 = yr+"-"+mnth;
//		Logging.writeToLog(false, "LOG_ARCHIVE_DIR date (Java8) = "+month1+" "+today1, Logging.DEBUG);
        
		////////////////////////////////////////////////////////////////////////////////		
		LOG_ARCHIVE_DIR = WORKING_DIR+SystemSettings.instance().getFileSeparator() +
				ARCHIVE_DIR + SystemSettings.instance().getFileSeparator() +
				month + SystemSettings.instance().getFileSeparator() +
				today + SystemSettings.instance().getFileSeparator();
	}


	/*
	 * The Server log starts with:	
	 * 			Start of Server main - version  x.y.z
	 * 
	 * The Client log starts with:	
	 * 			Start of Client <NewClient> - version x.y.z
	 * 
	 * ClientMain is initialised shortly after as
	 * 			Initialising ClientMain. runningFromJar =
	 */
	public static void writeToLog(boolean console, String msg, int level) {
		if (console) {
			if (level == ERROR || level == FATAL) {
				System.err.println(msg);
			} else {
				System.out.println(msg);
			}
			
			///////////////////////////////////////////////////////////////////////////
			//		DEBUGGING ONLY: Where does a specific Log msg come from?
			//		Use debugger with breakpoint on throw
			//
//			if (msg.contains("<body") || msg.contains("<html>")) {
//				try {
//					throw new Exception("Hunt for log output");
//				} catch(Exception e) {
//					String stacktrace = ExceptionUtils.getStackTrace(e);
//					Logging.writeToLog(true, "Found <body or <html>\nStackTrace:\n"+stacktrace, Logging.ERROR);
//				}
//			}
			///////////////////////////////////////////////////////////////////////////
		}

		// These ThreadIDs don't match Log4j
//		long threadID = Thread.currentThread().getId();
//		StringBuilder strBuilder = new StringBuilder();
//		strBuilder.append("JThread-"+threadID+" ");
//		strBuilder.append(msg);
//		msg = strBuilder.toString();
		
		currentLogger = LogManager.getLogger(CLIENT_LOGGER_NAME);
		writeToLog(msg, level);
	}

	private static void writeToLog(String msg, int level) {
		switch (level) {
		case DEBUG:
			currentLogger.debug(msg);
			break;

		case INFO:
			currentLogger.info(msg);
			break;

		case WARN:
			currentLogger.warn(msg);
			break;

		case ERROR:
			currentLogger.error(msg);
			break;

		case FATAL:
			currentLogger.fatal(msg);
			break;

		default:
			break;
		}

	}

	// Write to the i Log file - Only used by i classes
//	private static void writeToiLog(String msg, int level) {
//		switch (level) {
//		case DEBUG:
//			iLogger.debug(msg);
//			break;
//
//		case INFO:
//			iLogger.info(msg);
//			break;
//
//		case WARN:
//			iLogger.warn(msg);
//			break;
//
//		case ERROR:
//			iLogger.error(msg);
//			break;
//
//		case FATAL:
//			iLogger.fatal(msg);
//			break;
//
//		default:
//			break;
//		}
//
//	}

	/*
	 *  Called by client on pushing Bug Report button.
	 *  Show popup to collect reason, then zip up current client log.
	 *  FTP this file and the latest NUM_ARCHIVE_LOGS client archived files to the server and
	 *  send SendZippedLogs msg to server. Archived logs are already compressed.
	 */
//	public static void clientPanicAction(int pxID) {
//		if (errLogEmailRecipient == null) {
//			writeToLog(true, "No log error email recipient. Please check Server Log Maintenance Panel", ERROR);
//			return;
//		}
//		
//		String  reason = "CurrentPatient = "+pxID+" s/w version = "+Version.VERSION;
//		String dialogTxt = "Pushing OK will zip up logs and send to "+errLogEmailRecipient+
//				"\nPlease enter the reason you pushed the \"Bug report\" button on patient "+pxID;
//		String errTxt = "You must enter a reason for pushing the BugReport button";
//		String userInput = null;
//		int loopLimit = 0, LOOPMAX = 3;
//		
//		while (true && ++loopLimit <= LOOPMAX) {					
//			userInput = JOptionPane.showInputDialog(null, dialogTxt, "Reason for Sending Report", JOptionPane.WARNING_MESSAGE);
//			if (userInput == null) { 						// If user pushes Cancel, userInput = null
//				break;
//			} else if (userInput.isEmpty() && loopLimit < LOOPMAX) {
//				JOptionPane.showMessageDialog(null, errTxt, "Reason for Sending Report", JOptionPane.ERROR_MESSAGE);
//			} else if (!userInput.isEmpty()) {
//				break;
//			}
//		}
//		
//		if (loopLimit >= LOOPMAX) {
//			userInput = null;
//		} else {
//			reason += ". "+userInput;
//		}
//		
//		/*
//		 *  Zip up the current log file and FTP to server
//		 *  The zipped file will have same name as input file with .zip extension
//		 *  and will be stored temporarily in .../Clientlogs/name.zip
//		 *  Delete this after sending to server.
//		 */
//		if (userInput != null) {		// reason is null if Cancel pressed
//			String clientID = SpecificClientData.instance().getClientID();
//			String logID = clientID.replace(" ", "")+"_";			// Remove spaces and add '_'
//			
//			resetArchiveDir();
//			String[] latestArchivedLogs = getLatestArchiveLogs(true, LOG_ARCHIVE_DIR);
//			
//			// Send current Log file
//			GZipFile gzFile = new GZipFile(CURRENT_CLIENT_DIR, CURRENT_CLIENT_DIR);
//			FileFTP ftp = new FileFTP(ServerCatalogue.FTP_USERNAME, ServerCatalogue.FTP_PASSWORD);
//
//			// Send archived Log files
//			for (int i = 0; i <= NUM_ARCHIVE_LOGS; i++) {
//				String inFile = null, dir = null, zippedFile = null;
//				int zippedFileSize = 0;
//				
//				if (i == 0) {		// Current log file
//					inFile = CURRENT_CLIENT_LOG;
//					dir = CURRENT_CLIENT_DIR;
//					
//					if (new File(dir+inFile).exists()) {
//						zippedFileSize = gzFile.gzipThisFile(inFile);			// Zip the file
//					} else {
//						Logging.writeToLog(false, "Can't zip log file "+dir+inFile+" as it doesn't exist.", Logging.WARN);
//					}
//					
//					zippedFile = dir + inFile.replace("log", "gz");
//				} else {			// Archived log files
//					if (latestArchivedLogs == null) {
//						continue;
//					} else {
//						zippedFile = latestArchivedLogs[i-1];
//						if (zippedFile == null || zippedFile.isEmpty()) {
//							continue;
//						}
//					}
//					
//					File tmpFile = new File(zippedFile);
//					zippedFileSize = (int) tmpFile.length();
//				}
//			
//				ftp.sendZippedFile(zippedFile, zippedFileSize, logID);				// FTP it to server
//			}
//			
//			sendLogMsgToServer(reason);
//		}
//	}
//	
//	public static void sendLogMsgToServer(String reason) {
//			// Send 'SendZippedLogs' msg to server with the zipped filenames
//			ArrayList<String[]> dataElementValues = new ArrayList<String[]>();
//			String[] clientReason = {reason};
//			dataElementValues.add(clientReason);
//			String[] user = {ClientMain.getCurrentLoggedOnUser()};
//			dataElementValues.add(user);
//
//			ServerTask svt = ClientMain.instance().createTaskForServer("SendZippedLogs", "Datagram", "1", dataElementValues);
//			String xmlStr = svt.createXMLFromTask();
//			ClientMain.instance().addTaskToServerQueue(xmlStr);	
//	}
//
//	/*
//	 * Called by client AND server to extract the n latest logs from the Log Archive
//	 * specified in arcDir.
//	 * Logs are archived as for example:
//	 * .../2017-06/2017-06-04/ClientLogs.1.gz, .../2017-06/2017-06-04/ClientLogs.2.gz
//	 */
//	private static String[] getLatestArchiveLogs(boolean clientCalling, String arcDir) {
//		String[] arcLogs = new String[NUM_ARCHIVE_LOGS];
//		File logArcDir = new File(arcDir);
//		File[] files = logArcDir.listFiles();
//
//		if (files == null) {
//			Logging.writeToLog(false, "No files found in log archive dir: "+arcDir+" in getLatestArchiveLogs()", Logging.WARN);
//			return null;
//		} else {
//			Logging.writeToLog(false, files.length+" files found in log archive dir: "+arcDir+" in getLatestArchiveLogs()", Logging.INFO);
//		}
//		
//		// Sort files on creation data
//		sortFilesOnCreationDate(files);
//		
//		/*
//		 *  Process files found
//		 *  If clientCalling = true, Select the latest NUM_ARCHIVE_LOGS files with 
//		 *  "ClientLogs" in the name. Otherwise select the latest NUM_ARCHIVE_LOGS files with
//		 *  "ServerLogs" in the name.
//		 *  
//		 *  Archived logs have name in form: ClientLogs.n-2017-06-04.gz. 
//		 *  The files with lowest number are the latest, so select files as
//		 *  ClientLogs.1-2017-06-04.gz, ClientLogs.2-2017-06-04.gz, ClientLogs.NUM_ARCHIVE_LOGS-2017-06-04.gz
//		 *  Similarly for Server logs.
//		 *  
//		 */
//		
//		int count = 0;
//		
//		for (File oneFile : files) {
//			if (!oneFile.isFile()) {
//				continue;
//			}
//
//			String fileName = oneFile.getName();				// Name only
////			String filePath = Utils.getFilePath(oneFile);		// Path only
//			Path filePath = Paths.get(oneFile.getAbsolutePath());
//			
//			if (clientCalling) {
//				if (!fileName.contains("ClientLog")) {
//					continue;
//				}
//			} else {
//				if (!fileName.contains("ServerLog")) {
//					continue;
//				}
//			}
//			
//			arcLogs[count] = filePath.toString();
//			if (++count >= NUM_ARCHIVE_LOGS) {
//				break;
//			}
//		}
//		
//		return arcLogs;
//	}
//	
//	
//	// Sort files on creation data
//	public static void sortFilesOnCreationDate(File[] files) {
//		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
//	}
//	
//	/*
//	 * This is called by the server on receipt of client's SendZippedLogs message.
//	 * The Client's zipped logs will already be in inFilePath.
//	 * Zip up the server logs, including Activity log and put in same place.
//	 * Then send email with all zipped logs as attachments.
//	 */
//	public static void sendZippedLogsToUser(String reason) {
//		String workingDir = SystemSettings.instance().getWorkingDir();
//		String serverFTPDir = ServerConfigData.instance().getServerZipLogDir();
//
//		resetArchiveDir();
//		String[] latestArchivedLogs = getLatestArchiveLogs(false, LOG_ARCHIVE_DIR);
//
//		// Email current Log file
//		GZipFile gzFile = new GZipFile(CURRENT_SERVER_DIR, serverFTPDir);
//		
//		String inFile = null, dir = null, zippedFile = null;
//		
//		// Send archived Logs
//		for (int i = 0; i <= NUM_ARCHIVE_LOGS; i++) {
//			int zippedFileSize = 0;
//			
//			if (i == 0) {		// Current log file And Activity file
//				for (int j = 0; j < 2; j++) {
//					inFile = currentServerLogs[j];
//					dir = CURRENT_SERVER_DIR;
//					
//					// Zipped file is left in input dir as *.gz
//					if (new File(dir+inFile).exists()) {
//						zippedFileSize = gzFile.gzipThisFile(inFile);			// Zip the file
//					} else {
//						Logging.writeToLog(false, "Can't zip log file "+dir+inFile+" as it doesn't exist.", Logging.WARN);
//					}
//					
////					zippedFile = dir + inFile.replace("log", "gz");
//				}
//			} else {			// Archived log files
//				if (latestArchivedLogs == null) {
//					continue;
//				} else {
//					zippedFile = latestArchivedLogs[i-1];
//					if (zippedFile == null || zippedFile.isEmpty()) {
//						continue;
//					}
//				}
//
//				File tmpFile = new File(zippedFile);
//
//				int slashPos = zippedFile.lastIndexOf(SystemSettings.instance().getFileSeparator(), zippedFile.length()-1);
//				String nameOnly = zippedFile.substring(slashPos+1, zippedFile.length());
//				String newName = serverFTPDir + nameOnly;
//				File newFile = new File(newName);
//				
//				try {
//					FileUtils.copyFile(tmpFile, newFile);
//				} catch (Exception e) {
//					String stackTrace = ExceptionUtils.getStackTrace(e);
//					Logging.writeToLog(true, e.toString()+" trying to rename file " + tmpFile.getName()+" to "+newName+" StackTrace\n"+stackTrace, Logging.ERROR);
//					continue;
//				}
//			}
//		}
//
//		/*
//		 *  All zipped logs should now be in serverFTPDir
//		 *  Send email with the logs as attachments
//		 */
//		MailUtility smtpSender = null;
//		if (runningApplication == CLIENT) {
//			smtpSender = ClientMain.getSmsSmtpMailUtility();
//		} else {
//			smtpSender = ServerMain.getSmtpMailUtility();
//		}
//		
//		smtpSender.setSmtpDebug(false);
//		smtpSender.setUseSSL(true);
//		
//		// Set up attachments - all files in serverFTPDir
//		File zipDirFile = new File(serverFTPDir);
//		String files[] = zipDirFile.list();
//		
//		try {
//			for (int i=0; i<files.length; i++) {
//				Path zPath = FileSystems.getDefault().getPath(serverFTPDir, files[i]);
//				smtpSender.storeAttachment(zPath.toString());
//			}
//		} catch (Exception ex) {
//			String stackTrace = ExceptionUtils.getStackTrace(ex);
//			Logging.writeToLog(true, ex.toString()+" in Logging.sendZippedLogsToUser.\nStackTrace:\n"+stackTrace, Logging.ERROR);
//		}
//		
//		//sendEmail(String recipientAddr, String recipient, String subject, String textMsg)
//		String errMsg = smtpSender.sendEmail(errLogEmailRecipient, "Admin", "Bug Report logs", reason);
//		
//		if (errMsg != null) {
//			Logging.writeToLog(true, "Failed to send email with zipped logs "+errMsg, Logging.ERROR);
//		}
//
//		// Now delete all files from serverFTPDir
//		try {
//			FileUtils.cleanDirectory(zipDirFile);
//		} catch (Exception ex) {
//			String stackTrace = ExceptionUtils.getStackTrace(ex);
//			Logging.writeToLog(true, ex.toString()+" deleting zip file in Logging.sendZippedLogsToUser. StackTrace:\n"+stackTrace, Logging.ERROR);
//		}
//
//	}
//	
//	/* 
//	 * Search today's logs - current and archived for lineTests strings.
//	 * Email the results to errLogEmailRecipient
//	 * 
//	 * 1.	Check current Log file
//	 * 2.	Create a temp dir in file i/p dir e.g. LOG_ARCHIVE_DIR, CURRENT_CLIENT_DIR, CURRENT_SERVER_DIR
//	 * 3.	For Current log files (uncompressed):
//	 * 			Read files in current location and select required lines
//	 * 			Put selected lines into a file
//	 * 			Compress this file into the temp dir
//	 * 4.	For Archive files (already compressed):
//	 * 			Move all files from today's log to temp dir
//	 * 			Decompress gz files
//	 * 			Read uncompressed files and select required lines
//	 * 			Put selected lines into a file
//	 * 			Compress this file into the temp dir
//	 * 			Also scan ERROR grepLines and note the log filenames containing errors
//	 * 			Send these log files also
//	 * 	5.	Delete temp dir and all contents
//	 * 
//	 * NOTE: If running BOTH Client and Server on same machine don't set
//	 * LOG_MAINTENANCE_TIME for testing purposes to the same time
//	 * as they both use same temporary directory and one (Server) might delete files
//	 * before the other (Client) sends them leading to false 'FileNotFound' exceptions.
//	 * Test Client and Server components separately to avoid this.
//	 */
//	public void scanTodaysLogs() {
//		String emailSubject = null;
//		String startDirectoryStr = null;
//		File startDirectory = null;
//		String tmpLogFileName = null;
//		File clientTempLogDir = null;
//		File serverTempLogDir = null;
//
//		// Loop twice, k=0 for current logs and k=1 for archived logs
//		for (int k = 0; k < 2; k++) {
//
//			switch (k) {
//			case 0:						// Current Logs		*.log
//				if (runningApplication == CLIENT) {
//					startDirectoryStr = CURRENT_CLIENT_DIR;
//					startDirectory = new File(startDirectoryStr);
//					
//					tmpLogFileName = CURRENT_CLIENT_DIR+TEMP_LOG_DIR_NAME;
//					clientTempLogDir = new File(tmpLogFileName);
//					createDir(clientTempLogDir);
//					emailSubject = ClientMain.getClientIdentifier()+" v."+Version.VERSION+" Log ";
//				} else {
//					startDirectoryStr = CURRENT_SERVER_DIR;
//					startDirectory = new File(startDirectoryStr);
//					
//					tmpLogFileName = CURRENT_SERVER_DIR+TEMP_LOG_DIR_NAME;
//					serverTempLogDir = new File(tmpLogFileName);
//					createDir(serverTempLogDir);
//					emailSubject = "Server v."+Version.VERSION+" Log ";
//				}
//				
//				emailGrepLines(startDirectory, emailSubject, false);
//
//				// Now delete Temporary Directories and files
//				if (runningApplication == CLIENT) {
//					deleteTempFiles(clientTempLogDir);
//				} else {
//					deleteTempFiles(serverTempLogDir);
//				}
//				
//				break;
//				
//			case 1:						// Archived logs 	*.gz
//				resetArchiveDir();
//				String gzInputDir = LOG_ARCHIVE_DIR;					// Today's archived logs
//				startDirectoryStr = gzInputDir+TEMP_LOG_DIR_NAME+SystemSettings.instance().getFileSeparator();
//				startDirectory = new File(startDirectoryStr);
//				
//				createDir(startDirectory);
//
//				if (runningApplication == CLIENT) {
//					emailSubject = ClientMain.getClientIdentifier()+" v."+Version.VERSION+" Client Archive Log ";
//				} else {
//					emailSubject = "Server v."+Version.VERSION+" Archive Log ";
//				}
//				
//				ManageDocsInDB md = new ManageDocsInDB();		// to access ManageDocsInDB.getFilesInDir()
//				ArrayList<File> gzFiles = md.getFilesInDir(new File(gzInputDir));
//				
//				for (Iterator<File> fit = gzFiles.iterator(); fit.hasNext();) {
//					File gzFile = (File) fit.next();
//
////					Logging.writeToLog(true, "File to unzip: "+gzFile.getName()+" in dir "+gzInputDir, Logging.WARN);
//
//					if (gzFile.exists()) {	// unzip file into startDirectoryStr (raw files)
//						if (gzFile.getName().contains(".gz")) {
//							GZipFile gz = new GZipFile(startDirectoryStr, gzInputDir);
//							gz.gunzipFile(gzFile.getName());
//						}
//					} else {
//						Logging.writeToLog(true, emailSubject+" Can't unzip "+gzFile.getName()+" in dir "+gzInputDir+" as it doesn''t exist", Logging.ERROR);
//					}
//
//				}
//
//				emailGrepLines(startDirectory, emailSubject, true);
//
//				// Now delete Temporary Directories and files
//				deleteTempFiles(startDirectory);
//
//				break;
//			}
//		}
//
//	}
//
//	private void deleteTempFiles(File fileDir ) {
//		try {
//			FileUtils.deleteQuietly(fileDir);
//		} catch (Exception ex) {
//			String stackTrace = ExceptionUtils.getStackTrace(ex);
//			Logging.writeToLog(true, ex.toString()+" deleting temporary dir "+fileDir.toString()+" in Logging.deleteTempFiles().\nStackTrace:\n"+stackTrace, Logging.ERROR);
//		}
//	}
//	
//	private void createDir(File startDirectory) {
//		try {
//			if (!startDirectory.exists()) {
//				FileUtils.forceMkdir(startDirectory);
//			}
//		} catch(Exception e) {
//			Logging.writeToLog(true, e.toString()+" in Logging.createDir(): "+startDirectory.toString(), Logging.ERROR);
//		}
//		
//	}
//	
//	/*
//	 * For current logs, startDirectory doesn't include Temp dir.
//	 * But for Archived logs, it does.
//	 */
//	private void emailGrepLines(File startDirectory, String emailSubject, boolean archive) {
//		String [] lineTests	= {"ERROR", "WARN"};
//		String subject = null;
//		boolean clientProcessing = startDirectory.getPath().contains("ClientLogs");
//		
//		ManageDocsInDB md = new ManageDocsInDB();		// to access ManageDocsInDB.getFilesInDir()
//
//		// startDirectory now contains *.log files
//		ArrayList<File> files = md.getFilesInDir(startDirectory);
//
//		ArrayList<String> grepLines = new ArrayList<String>();
//
//		// Check each file against the grep lineTests
//		for (int i = 0; i < lineTests.length; i++) {
//			grepLines.clear();
//			subject = emailSubject+lineTests[i]+" lines";
//			
//			for (Iterator<File> fit = files.iterator(); fit.hasNext();) {
//				File file = (File) fit.next();
//
//				ArrayList<String> lines = collectGrepLines(file, lineTests[i]);
//				
//				for (Iterator<String> lit = lines.iterator(); lit.hasNext();) {
//					String line = (String) lit.next();
//					grepLines.add(file.getName()+": "+line);
//				}
//			}
//
//			String emailContent = grepLines.size()+" "+lineTests[i]+" lines";
//			Logging.writeToLog(false, emailContent, Logging.INFO);
//			
//			/*
//			 *  Put the lines into a temp file and compress it, then send
//			 *  the compressed file to errLogEmailRecipient as an attachment.
//			 */
//			String fileDir = null;
//			if (archive) {
//				fileDir = startDirectory.getPath()+SystemSettings.instance().getFileSeparator();			
//			} else {
//				fileDir = startDirectory.getPath()+SystemSettings.instance().getFileSeparator()+
//														TEMP_LOG_DIR_NAME+SystemSettings.instance().getFileSeparator();
//			}
//			
//			
//			/*
//			 * Scan WARN and ERROR grepLines (i = 0 and archive only) and store the file name in a Set (no duplicates)
//			 * Then add these files as attachments to the email
//			 */
//			Set<String> errLogs = null;
//			
//			if (archive) {
//				if (lineTests[i].equals("ERROR") || lineTests[i].equals("WARN")) { 
//					errLogs = getErrorLogs(grepLines);
//				}
//			} else {		// Send Current log for ERROR & WARN		if (lineTests[i].equals("ERROR")) {
//				String curFile = null, copFile = null;
//				errLogs = new HashSet<String>();
//				
//				if (clientProcessing) {
//					errLogs.add("ClientLog.log");
//					curFile = startDirectory.getPath()+SystemSettings.instance().getFileSeparator()+"ClientLog.log";
//					copFile = startDirectory.getPath()+SystemSettings.instance().getFileSeparator()+TEMP_LOG_DIR_NAME+SystemSettings.instance().getFileSeparator()+"ClientLog.log";
//				} else {
//					errLogs.add("ServerLog.log");
//					curFile = startDirectory.getPath()+SystemSettings.instance().getFileSeparator()+"ServerLog.log";
//					copFile = startDirectory.getPath()+SystemSettings.instance().getFileSeparator()+TEMP_LOG_DIR_NAME+SystemSettings.instance().getFileSeparator()+"ServerLog.log";
//				}
//				
//				// Copy current file to .../CheckedLogs dir
//				
//				Path curFilePath = Paths.get(curFile);
//				Path copFilePath = Paths.get(copFile);
//				
//				try {
//					Files.copy(curFilePath, copFilePath);
//				} catch (Exception e) {
//					String stackTrace = ExceptionUtils.getStackTrace(e);
//					Logging.writeToLog(true, e.toString()+" copying file in Logging.emailGrepLines\nStackTrace\n"+stackTrace, Logging.ERROR);
//				}
//			}
//			
//
//			/* 
//			 * Create a *.log file of the selected lines and put it in Temp dir
//			 */
//			String compFile = fileDir+TEMP_FILE_NAME;
//			Path out = Paths.get(compFile);
//			
////			Logging.writeToLog(true, "Writing grepped lines to: "+compFile, Logging.DEBUG);
//			
//			try {
//				Files.write(out, grepLines, Charset.defaultCharset());
//			} catch(Exception e) {
//				String stacktrace = ExceptionUtils.getStackTrace(e);
//				Logging.writeToLog(true, e.toString()+" in Logging.emailGrepLines()\nStackTrace:\n"+stacktrace, Logging.ERROR);
//			}
//
//			/* 
//			 * Compress the file. Its name will then change to *.gz
//			 */
//			GZipFile gz = new GZipFile(fileDir, fileDir);
//			gz.gzipThisFile(TEMP_FILE_NAME);
//
//			String tmpFileName = TEMP_FILE_NAME.replace("log", "gz");		// Change name to *.gz
//			
////			Logging.writeToLog(true, "tmpFileName= "+tmpFileName+" "+gz.rawFileDir+" "+gz.zippedFileDir, Logging.DEBUG);
//
//			MailUtility smtpSender = null;
//			if (runningApplication == CLIENT) {
//				smtpSender = ClientMain.getSmsSmtpMailUtility();
//			} else {
//				smtpSender = ServerMain.getSmtpMailUtility();
//			}
//			
//			smtpSender.setSmtpDebug(false);
//			smtpSender.setUseSSL(true);
//
//			// Set up attachment - 1 file: startDirectory.getPath()+ "/rawLines.log"
//			
//			try {
//				Path zPath = FileSystems.getDefault().getPath(fileDir, tmpFileName);
//				
////				Logging.writeToLog(true, "Attachment= "+zPath.toString(), Logging.DEBUG);
//
//				smtpSender.storeAttachment(zPath.toString());
//				
//				if (errLogs != null && errLogs.size() > 0) {
//					for (Iterator<String> it = errLogs.iterator(); it.hasNext();) {
//						String errFileName = (String) it.next();
//						
//						
//						gz.gzipThisFile(errFileName);
//						errFileName = errFileName.replace("log", "gz");		// Change name to *.gz
//						
//						Path ePath = FileSystems.getDefault().getPath(fileDir, errFileName);
//						smtpSender.storeAttachment(ePath.toString());
//					}
//					
//				}
//			} catch (Exception ex) {
//				String stackTrace = ExceptionUtils.getStackTrace(ex);
//				Logging.writeToLog(true, ex.toString()+" in Logging.emailGrepLines().\nStackTrace:\n"+stackTrace, Logging.ERROR);
//			}
//
//			String errMsg = smtpSender.sendEmail(errLogEmailRecipient, "Admin", subject, emailContent);
//
//			if (errMsg != null) {
//				Logging.writeToLog(true, "Failed to send email with error log lines "+errMsg, Logging.ERROR);
//			}
//		}			// End of lineTest loop  i.e. ERROR or WARN
//	}
//	
//	
//	private ArrayList<String> collectGrepLines(File file, String grepTest) {
//		ArrayList<String> matchingLines = new ArrayList<String>();
//		
//		LineIterator it = null;
//		try{
//			it = FileUtils.lineIterator(file, "UTF-8");
//			while (it.hasNext()){
//				String line = it.nextLine();
//				if(line.contains(grepTest)){
//					matchingLines.add(line);
//				}
//			}
//			
//		} catch (Exception e) {
//			String stackTrace = ExceptionUtils.getStackTrace(e);
//			Logging.writeToLog(true, e.toString()+" in Logging.collectGrepLines()\nStackTrace:\n"+stackTrace, Logging.ERROR);
//		} 
//		finally {LineIterator.closeQuietly(it);}
//
//		return matchingLines;
//	}
//
//	private Set<String> getErrorLogs(ArrayList<String> grepLines) {
//		Set<String> errLogs = new HashSet<String>();
//
//		for (Iterator<String> it = grepLines.iterator(); it.hasNext();) {
//			String errLine = (String) it.next();
//			int colon = errLine.indexOf(":");
//			if (colon > -1) {
//				String name = errLine.substring(0, colon);
//				name.replace(".log", ".gz");
//				errLogs.add(name);
//			}
//
//		}
//
//		return errLogs;
//	}
//	
//	
//////////////////////////////////////////////////////////////////////////////////////////////
////			INNER CLASS - Grep   NOT USED
//////////////////////////////////////////////////////////////////////////////////////////////
//			
//	public class Grep extends DirectoryWalker
//	{
//		private String grepTest;
//		ArrayList<String> matchingLines = new ArrayList<String>();
//		
//		public Grep(IOFileFilter filter){
//			super();
////			super(filter, 1);				// Only look at currrent level
//
//		}
//
//		// Returns ArrayList of files as Objects		
//		public ArrayList<File> findFiles(File startDirectory){
//			ArrayList<File> files = new ArrayList<File>();
//			try {
//				walk(startDirectory, files);
//			} catch (Exception e) {
//				String stackTrace = ExceptionUtils.getStackTrace(e);
//				Logging.writeToLog(true, e.toString()+" in Logging.Grep.clean()\nStackTrace:\n"+stackTrace, Logging.ERROR);
//			}
//
//			return files;
//		}
//
//		// Called for each directory found by walk()
//		@Override
//		protected boolean handleDirectory(File directory, int depth, Collection results){
//			// Decide if a (sub) directory will be handled for recursive search
//			return true;
//		}
//
//		// Called for each file found by walk(). Matching lines are in results
//		@Override
//		protected void handleFile(File file, int depth, Collection results)
//		{
//			LineIterator it = null;
//			try{
//				it = FileUtils.lineIterator(file, "UTF-8");
//				while (it.hasNext()){
//					String line = it.nextLine();
//					if(line.matches(grepTest)){
//						results.add(file);
//						matchingLines.add(line);
//					}
//				}
//			} catch (Exception e) {
//				String stackTrace = ExceptionUtils.getStackTrace(e);
//				Logging.writeToLog(true, e.toString()+" in Logging.Grep.handleFile()\nStackTrace:\n"+stackTrace, Logging.ERROR);
//			} 
//			finally {LineIterator.closeQuietly(it);}
//		}
//
//		
//		public void setGrepTest(String grepTest) {
//			this.grepTest = grepTest;
//		}
//
//		public ArrayList<String> getMatchingLines() {
//			return matchingLines;
//		}
//	}
//////////////////////////////////////////////////////////////////////////////////////////////	
//	/*
//	 *  The following class is not used.
//	 *  Because the generated Configuration file still has to be put on classpath manually
//	 *  Use external file: Log4j2-Config/log4j2.xml instead
//	 */
/////////////////////////////////////////////////////////////////////////////////////////////
	
//		class Log4j2Config {
//			private static final String LOG4J_CONFIGURATION_DIR		= "Log4jConfiguration";
//			private static final String LOG4J_CONFIGURATION_FILE	= "log4j2.xml";		// DON'T CHANGE!!!
//
//			Log4j2Config() {
//				// First create dir Log4jConfiguration
//				File f = new File(LOG4J_CONFIGURATION_DIR);		// Incorrect! but not used
//				if (!f.exists()) {
//					try {
//						FileUtils.forceMkdir(f);
//					} catch(Exception e) {
//						Logging.writeToLog(true, e.toString()+" in Logging() creating dir: "+LOG4J_CONFIGURATION_DIR, Logging.ERROR);
//					}
//				}
//
//				// Now create config file if not present and copy configuration data into it
//				//	String congigFileName = LOG4J_CONFIGURATION_DIR+SystemSettings.instance().getFileSeparator()+LOG4J_CONFIGURATION_FILE;
//				String congigFileName = LOG4J_CONFIGURATION_DIR+"/"+LOG4J_CONFIGURATION_FILE;
//				File cf = new File(congigFileName);
//				if (!cf.exists()) {
//					createConfigFile(congigFileName);
//				}
//			}
//
//			/*
//			 * If ANYTHING in xmlLines changes, delete the configuration file in
//			 * .../i-base/LOG4J_CONFIGURATION_DIR/log4j2.xml and restart application.
//			 */
//			private void createConfigFile(String fName) {
//				String [] xmlLines = {
//						"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
//						"<!-- Change to INFO for Prod -->",
//						"<Configuration status=\"DEBUG\" name=\"setupLog\">",
//						"	<Properties>",
//						"		<Property name=\"SERVER_LOG_DIR\">ServerLogs</Property>",
//						"		<Property name=\"ACTIVITY_LOG_DIR\">ServerLogs</Property>",
//						"		<Property name=\"CLIENT_LOG_DIR\">ClientLogs</Property>",
//						"		<Property name=\"i_LOG_DIR\">Client-i-Logs</Property>",
//						"		<Property name=\"PATTERN\">%d %-5p %t - %m%n</Property>",
//						"	</Properties>",
//						"	<Appenders>",
//						"		<RollingFile name=\"serverAppender\" fileName=\"${SERVER_LOG_DIR}/ServerLog4joutput.log\" filePattern=\"${SERVER_LOG_DIR}/ServerLog4joutput.log.%i.log\">",
//						"			<PatternLayout pattern=\"${PATTERN}\"/>",
//						"			<Policies>",
//						"				<SizeBasedTriggeringPolicy size=\"2MB\" max=\"10\"/>",
//						"			</Policies>",
//						"		</RollingFile>",
//						"		<RollingFile name=\"activityFile\" fileName=\"${SERVER_LOG_DIR}/ActivityLog4joutput.log\" filePattern=\"${SERVER_LOG_DIR}/ActivityLog4joutput.log.%i.log\">",
//						"			<PatternLayout pattern=\"${PATTERN}\"/>",
//						"			<Policies>",
//						"				<SizeBasedTriggeringPolicy size=\"2MB\" max=\"10\"/>",
//						"			</Policies>",
//						"		</RollingFile>",
//						"		<RollingFile name=\"clientAppender\" fileName=\"${CLIENT_LOG_DIR}/ServerLog4joutput.log\" filePattern=\"${CLIENT_LOG_DIR}/ServerLog4joutput.log.%i.log\">",
//						"			<PatternLayout pattern=\"${PATTERN}\"/>",
//						"			<Policies>",
//						"				<SizeBasedTriggeringPolicy size=\"2MB\" max=\"10\"/>",
//						"			</Policies>",
//						"		</RollingFile>",
//						"		<RollingFile name=\"iAppender\" fileName=\"${i_LOG_DIR}/Client-i-Log4joutput.log\" filePattern=\"${i_LOG_DIR}/ServerLog4joutput.log.%i.log\">",
//						"			<PatternLayout pattern=\"${PATTERN}\"/>",
//						"			<Policies>",
//						"				<SizeBasedTriggeringPolicy size=\"2MB\" max=\"10\"/>",
//						"			</Policies>",
//						"		</RollingFile>",
//						"		<RollingFile name=\"rootAppender\" fileName=\"/RootLog4joutput.log\" filePattern=\"${i_LOG_DIR}/ServerLog4joutput.log.%i.log\">",
//						"			<PatternLayout pattern=\"${PATTERN}\"/>",
//						"			<Policies>",
//						"				<SizeBasedTriggeringPolicy size=\"2MB\" max=\"10\"/>",
//						"			</Policies>",
//						"		</RollingFile>",
//						"	</Appenders>",
//						"	<Loggers>",
//						"		<Root level=\"INFO\">",
//						"			<AppenderRef ref=\"rootAppender\"/>",
//						"		</Root>",
//						"		<Logger name=\"clientLogger\" level=\"DEBUG\" additivity=\"false\">",
//						"			<AppenderRef ref=\"clientAppender\"/>",
//						"		</Logger>",
//						"		<Logger name=\"serverLogger\" level=\"DEBUG\" additivity=\"false\">",
//						"			<AppenderRef ref=\"serverAppender\"/>",
//						"		</Logger>",
//						"		<Logger name=\"activityLogger\" level=\"DEBUG\" additivity=\"false\">",
//						"			<AppenderRef ref=\"activityFile\"/>",
//						"		</Logger>",
//						"		<Logger name=\"iLogger\" level=\"DEBUG\" additivity=\"false\">",
//						"			<AppenderRef ref=\"iAppender\"/>",
//						"		</Logger>",
//						"	</Loggers>",
//				"</Configuration>"};
//
//				Path file = Paths.get(fName);
//				ArrayList<String> configLines = new ArrayList<String>();
//				for (int i = 0; i < xmlLines.length; i++) {
//					configLines.add(xmlLines[i]);
//				}
//
//				try {
//					Files.write(file, configLines, StandardCharsets.UTF_8);
//				} catch(Exception e) {
//					String stackTrace = ExceptionUtils.getStackTrace(e);
//					Logging.writeToLog(true, e.toString()+" creating Log4j Configuration file in Logging.createConfigFile. StackTrace:\n"+stackTrace, Logging.ERROR);
//				}
//				return;
//			}
//		}
//
//		public static String getErrLogEmailRecipient() {
//			return errLogEmailRecipient;
//		}
//
//		public static void setErrLogEmailRecipient(String errLogEmailRecipient) {
//			Logging.errLogEmailRecipient = errLogEmailRecipient;
//		}
}
