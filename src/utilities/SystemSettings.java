package utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * A singleton class to hold system information
 * @author Tony Cradwick
 * @version 14/9/06
 */
public final class SystemSettings {

	private static SystemSettings _instance = null;

	private int OS = -1;
	private String OSText = null;
	private String homeDirectory = null;
	private String workingDirectory = null;
	private String jarDirectory = null;
	private String driveSerialNumber = null;

	private final String fileSeparator = System.getProperty("file.separator");					// '/' on Unix; '\' on Windows
	private final String lineTerminator = System.getProperty("line.separator");				// '\n' on Unix; '\r\n' on Windows
	public static final int WINDOWS = 0;
	public static final int MAC = 1;
	public static final int LINUX = 2;

    
	/**
	 *
	 * @return returns the single instance of this class
	 */
	public static SystemSettings instance() {
		if (_instance == null){
			_instance = new SystemSettings();
		}
		return _instance;
	}



	/**
	 * prevent instantiation
	 * but sets up all the system settings in the class
	 */
	private SystemSettings () {
		//set home directory
    this.homeDirectory = (System.getProperty("user.home") + this.fileSeparator);
    this.workingDirectory = (System.getProperty("user.dir"));
    this.jarDirectory = workingDirectory + this.fileSeparator + "ExecutableJars";

		//set OS
		this.OSText = System.getProperty("os.name").toUpperCase();
		if (this.OSText.indexOf("WINDOWS") != -1){
			OS = WINDOWS;
			//set HDD serial number
			this.driveSerialNumber = new VolumeSerialNumberGetter().collectHDDSerialNumber();
		}
		else if (OSText.indexOf("LINUX") != -1) {
		  //don't need to set HDD serial number for linux as this is not used any more - dongle security instead
		  OS = LINUX;
		  //this.driveSerialNumber = new VolumeSerialNumberGetter().collectHDDSerialNumber();
		}
		

	}

	/***********************************************************************************************************
	 ******************************************* The public methods ********************************************
	 **********************************************************************************************************/


	/**
	 * @return the oS
	 */
  public int getOS()
  {
    return this.OS;
	}

	/**
	 * @param os the oS to set
	 */
	public String getOSText() {
		return this.OSText;
	}

	public String getHomeDir() {
    return this.homeDirectory;
	}

	public String getWorkingDir() {
    return this.workingDirectory;
	}

	//NOTE: Fonts are hidden in the 'SOUNDS' dir under the name blipLong.wav
  public String getFontsDir()
  {
    return this.workingDirectory + this.fileSeparator + "bin" + this.fileSeparator + 
      "sounds" + this.fileSeparator;
	}

	public String getChartsDir() {
    return this.workingDirectory + this.fileSeparator + "bin" + this.fileSeparator + 
      "charts" + this.fileSeparator;
  }

  public String getSoundsDir() {
    return this.workingDirectory + this.fileSeparator + "bin" + this.fileSeparator + 
      "sounds" + this.fileSeparator;
  }

  public String getMediaDir() {
    return this.workingDirectory + this.fileSeparator + "bin" + this.fileSeparator + 
      "animation" + this.fileSeparator;
	}
	
	 public String getBinDir() {
    return this.workingDirectory + this.fileSeparator + "bin" + this.fileSeparator;
	  }

	public String getHDDSerialNumber() {
    return this.driveSerialNumber;
	}

  public String getFileSeparator()
  {
    return this.fileSeparator;
  }







	/**
	 *  Gets the HD serial number
	 *
	 * @author     CEHJ
	 * @created    25 March 2004
	 */
	private final class VolumeSerialNumberGetter {
	  StringBuffer collector;
	  
    private VolumeSerialNumberGetter()
    {
    }
    
	  public String collectHDDSerialNumber() {
	  	String serialNumber = "unavailable";

	  	String[] args = (String[])null;
	  	
	    try {
	  	  if (OS == SystemSettings.WINDOWS) {
	  		  args = new String[] { "cmd.exe",  "/c",  "dir", "|",  "find", "/i",  "Serial Number is" };
	  	  } else if (OS == SystemSettings.LINUX) {
	  		  args = new String[] { "bash",  "/",  "dir", "|",  "find", "/i",  "Serial Number is" };
	  	  }
	  	  
	      final Process pro = Runtime.getRuntime().exec(args);

	      final InputStream output = pro.getInputStream();

	      this.collector = new StringBuffer();
	      final ProcessStreamReader collectingReader = new ProcessStreamReader(output);
	      final Thread out = new Thread(collectingReader);
	      out.start();
	      out.join();
	      String volumeNumber = this.collector.toString();
	      volumeNumber = volumeNumber.substring(volumeNumber.lastIndexOf(' '));
	      serialNumber = volumeNumber.trim();
	    } catch (final java.io.IOException e) {
	      e.printStackTrace();
	    } catch (final java.lang.InterruptedException e) {
	      e.printStackTrace();
	    } finally {
	    }
	    return serialNumber;
	  }

	  class ProcessStreamReader implements Runnable {

	    InputStream is;

	    public ProcessStreamReader(final InputStream is) {
	      this.is = is;
	    }

	    public void run() {
	      try {
	        final BufferedReader in = new BufferedReader(new InputStreamReader(is));
	        String temp = null;
	        while ((temp = in.readLine()) != null) {
	          collector.append(temp);
	        }
	        is.close();
	      }
	      catch (final Exception e) {
	        e.printStackTrace();
	      }
	    }
	  }

	}







	public String getJarDirectory() {
		return jarDirectory;
	}

	public void setJarDirectory(String jarDirectory) {
		this.jarDirectory = jarDirectory;
	}



	public synchronized String getLineTerminator() {
		return lineTerminator;
	}
}
