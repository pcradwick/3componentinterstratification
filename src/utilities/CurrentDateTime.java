package utilities;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

//import com.google.api.client.util.DateTime;
//import server.workers.ServerActivityCategories;

public class CurrentDateTime {
	private Timer timerThread = null;

	Calendar dtNow = null;
	Date timeNow = null;

	String date = null;						// yyyy-MM-dd
	String time = null;						// HH:mm:ss			24 hr
	String dateTime = null;

	int year = -1, month = -1, day = -1, hour = -1, mins = -1, secs = -1;

	/////////////////////////////////////////////////////////////////////////////
	//				Constructors
	/////////////////////////////////////////////////////////////////////////////

	//		Calculate all elements of current time
	public CurrentDateTime() {
		dtNow = Calendar.getInstance();
		splitDate(dtNow);
	}

	//		Calculate all elements of supplied time, dt
	public CurrentDateTime(Calendar dt) {
		dtNow = dt;
		splitDate(dtNow);
	}

	/*
	 *  Constructor for using Timer
	 *  Create a timer to run for tt seconds, but don't start it
	 */
	public CurrentDateTime(int tt) {
		timerThread = new Timer(tt);
	}

	
	////////////////////////////////////////////////////////////////////////////////
	private void splitDate(Calendar cal) {
		year = cal.get(Calendar.YEAR);
		month = cal.get(Calendar.MONTH);			// Jan = 0, but log uses Jan = 1 !!!
		day = cal.get(Calendar.DATE);
		hour = cal.get(Calendar.HOUR_OF_DAY);		// 24 hr
		mins = cal.get(Calendar.MINUTE);
		secs = cal.get(Calendar.SECOND);
		timeNow = cal.getTime();

		date = new SimpleDateFormat("yyyy-MM-dd").format(timeNow); 
		time = new SimpleDateFormat("HH:mm:ss").format(timeNow); 
		dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(timeNow); 
	}

	/*
	 *  Add various units to date dtNow (positive or negative)
	 *  If the default constructor is used, the starting date is NOW
	 *  If the parameterised constructor is used, 
	 *  the starting date is the date supplied.
	 *  In both cases use the getters to get the individual items.
	 */
	public Calendar addTime(int unit, int quantity) {
		Calendar cal = dtNow;

		switch (unit) {
//		case ServerActivityCategories.MINUTE_FILTER:
//			cal.add(Calendar.MINUTE, quantity);
//			break;
//
//		case ServerActivityCategories.HOUR_FILTER:
//			cal.add(Calendar.HOUR_OF_DAY, quantity);
//			break;
//
//		case ServerActivityCategories.DAY_FILTER:
//			cal.add(Calendar.DAY_OF_MONTH, quantity);
//			break;
//
//		case ServerActivityCategories.WEEK_FILTER:
//			cal.add(Calendar.WEEK_OF_YEAR, quantity);
//			break;
//
//		case ServerActivityCategories.MONTH_FILTER:
//			cal.add(Calendar.MONTH, quantity);
//			break;
			
		case Calendar.MINUTE:
			cal.add(Calendar.MINUTE, quantity);
			break;

		case Calendar.HOUR_OF_DAY:
			cal.add(Calendar.HOUR_OF_DAY, quantity);
			break;

		case Calendar.DAY_OF_MONTH:
			cal.add(Calendar.DAY_OF_MONTH, quantity);
			break;

		case Calendar.WEEK_OF_YEAR:
			cal.add(Calendar.WEEK_OF_YEAR, quantity);
			break;

		case Calendar.MONTH:
			cal.add(Calendar.MONTH, quantity);
			break;
			
		case Calendar.DAY_OF_WEEK:
			cal.add(Calendar.DAY_OF_WEEK, quantity);
		}

		splitDate(cal);
		return cal;
	}	

	/*
	 * Returns true if testDate (email, txt or print) plus 'window' is after Now
	 * i.e. Now is inside the Time window: 		testDate....testDate+window
	 */
	public boolean checkTimeWindow(Date testDate, Date dateNow, int window) {
		CurrentDateTime cdt = new CurrentDateTime();
		Calendar now = cdt.dateToCalendar(dateNow);
		
		Calendar ecal = cdt.dateToCalendar(testDate);
		CurrentDateTime ecdt = new CurrentDateTime(ecal);
		ecal = ecdt.addTime(Calendar.DAY_OF_MONTH, window);
		
//		String msg = "testDate = "+testDate.toString()+" Now = "+dateNow.toString()+" Window = "+ecal.getTime().toString();
//		Logging.writeToLog(false, msg, Logging.WARN);
		
		boolean  inside = false;
		if (window > 0) {
			inside = ecal.compareTo(now) > 0;
		} else if (window < 0) {
			inside = ecal.compareTo(now) < 0;
		}
		
		return inside;
	}

	public String dateToStr(java.util.Date juDate) {
		String strDate = null;
		
		if (juDate != null) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			strDate = dateFormat.format(juDate);
		}
		
		return strDate;
	}

	public Calendar dateToCalendar(java.util.Date juDate) {
		return strToCalendar(dateToStr(juDate));
	}
	
	/*
	 * The server sends null dates as 0000-00-00
	 */
	public java.util.Date strToDate(String strDate) {
		Date date = null;
		
		if (strDate != null && strDate != "" && strDate != "0000-00-00") {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			try {
				date = sdf.parse(strDate);
			} catch (ParseException p) {
				Logging.writeToLog(true, p.toString()+" in CurrentDateTime.strToDate(). i/p= "+strDate, Logging.ERROR);
			}
		}
		
		return date;
	}

	/*
	 * Time expects a string of the format HH:mm:ss
	 * If no secs, add :00
	 */
	public Time strToTime(String strTime) {
		Time  time = null;
		
		int lastColonPos = strTime.lastIndexOf(":");
		int firstColonPos = strTime.indexOf(":");
		if (lastColonPos == firstColonPos) {
			strTime += ":00";
		}

		if (strTime != null && strTime != "") {
			try {
				time = Time.valueOf(strTime);
			} catch (Exception p) {
				Logging.writeToLog(true, p.toString()+" in CurrentDateTime.strToTime(). i/p= "+strTime, Logging.ERROR);
			}
		}
		
		return time;
	}

	/*
	 * Times input as 24hr will be returned as 24hr
	 */
	public Timestamp strToTS(String strDate) {
		Timestamp  tsDate = null;
		
		if (strDate != null && strDate != "") {
			try {
				tsDate = Timestamp.valueOf(strDate);
			} catch (Exception p) {
				Logging.writeToLog(true, p.toString()+" in CurrentDateTime.strToTS(). i/p= "+strDate, Logging.ERROR);
			}
		}
		
		return tsDate;
	}

	public static java.sql.Date convertUtilDateToSqlDate(java.util.Date utilDate) {
		java.sql.Date sqlDate = null;
		if (utilDate != null) {
			sqlDate = new java.sql.Date(utilDate.getTime());
		}
		return sqlDate;
	}

	public Calendar strToCalendar(String strDate){
		Calendar cal = Calendar.getInstance();

		if (strDate != null && strDate != "") {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			
			try {
				cal.setTime(sdf.parse(strDate));
			} catch (ParseException p) {
				Logging.writeToLog(true, p.toString()+" in CurrentDateTime.strToCalendar(). i/p= "+strDate, Logging.ERROR);
			}
		}

		return cal;
	}

	public Date msecToDate(long msec) {
		return new Date(msec);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	//Thread to run for a specified time (secs)
	// NOTE: ClientMain.restartLogonTimer() interrupts this thread
	//			every time the mouse is moved - checking for no activity
	//			hence ignore InterruptedException
	////////////////////////////////////////////////////////////////////////////////////////

	public class Timer extends Thread {
		private long time;						// Timer delay in secs
		private boolean running = false;

		/** Create a new timer */
		public Timer(long time) {
			this.time = time * 1000;
		}

		public void run() {
			setRunning(true);
			try {
				sleep(this.time);
			} catch (InterruptedException e) {
//				Logging.writeToLog(false, e.toString()+" Timer thread sleep "+e.getMessage(), Logging.WARN);
			}

			setRunning(false);
		}

		public void stopTimer() {
			this.interrupt();
		}

		public void setTime(long time) {
			this.time = time * 1000;
		}


		public boolean isRunning() {
			return running;
		}

		public void setRunning(boolean running) {
			this.running = running;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// Functions for controlling timerThread
	////////////////////////////////////////////////////////////////////////////////

	/*
	 * Using the timer thread
	 * CurrentDateTime cdTimer = new CurrentDateTime(TIMEOUT);		// Create timer thread
	 * cdTimer.startTimer();										// Start Timer thread
	 * 
	 * OR:
	 * 
	 * CurrentDateTime cdTimer = new CurrentDateTime();				// Create timer thread
	 * cdTimer.startTimer(TIMEOUT);									// Start Timer thread
	 */
	
	// See constructor above
	
	// Or could use ThreadStatus class
	public boolean isFinished() {
		if (timerThread.getState() == Thread.State.TERMINATED) {
			return true;
		}

		return false;
	}

	// Start timer thread
	public void startTimer() {
		timerThread.start();
	}

	public void startTimer(long time) {
		timerThread.setTime(time);
		timerThread.start();
	}

	// Stop the timer thread (timer)
	public void stopTimer() {
		timerThread.stopTimer();
		timerThread.setRunning(false);
	}

	public Timer getTimerThread() {
		return timerThread;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	
	public Date getTimeNow() {
		return timeNow;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public int getHour() {
		return hour;
	}

	public int getMins() {
		return mins;
	}

	public String getDate() {
		return date;
	}

	public String getDateTime() {
		return dateTime;
	}

	public String getTime() {
		return time;
	}

	public int getSecs() {
		return secs;
	}

	public void setSecs(int secs) {
		this.secs = secs;
	}
}
