package main;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;

import formfactors.OneConstantsRow;
import formfactors.RStarAndF;
import formfactors.ReadFile;
import layerSF.LayerComponent;
import layerSF.OneRStarSums;


public class Strat3GUI extends JFrame {
	
	private JDesktopPane desktop;
	private int numComponents = 0;
	private double maxRStar = 0.75;
	private ArrayList<LayerComponent> layers = null;
	private ArrayList<OneRStarSums> layerStructureFactorSums = null;
	private ArrayList<OneConstantsRow> formFactorConst = null;
	private ArrayList<String> labels = null;

	private Set<String> uniqueAtoms;		// Set of unique atom types
	private Map<String, ArrayList<RStarAndF>> fCurves= new HashMap<String, ArrayList<RStarAndF>>();
	
	/*
	 *  Structure Factor sums evaluated in CalculateLayerSF
	 * 		V(kk) and V(kl) 	ClayMinerals (1975), 10, 347
	 * 		V(A)**2, V(B)**2, V(C)**2, C(AB), S(AB), C(AC), S(AC),
	 * 		C(AC), S(AC)		Clay Minerals (1978), 13, 53
	 * 
	 * Store <r*, Sum>
	 */
	
	private ArrayList<OneRStarSums> sfSums = new ArrayList<OneRStarSums>();		// Double[9]
	
	public Strat3GUI() {
		super("Three Component Interstratification.     version "+Strat3Main.getVersion());
		
		layers = new ArrayList<LayerComponent>();
		layerStructureFactorSums = new ArrayList<OneRStarSums>();
		uniqueAtoms = new HashSet<String>();
		
       int inset = 200;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(inset, inset,
                  screenSize.width  - inset*2,
                  screenSize.height - inset*2);

		desktop = new JDesktopPane();
		setContentPane(desktop);

		// Set Menu
		MenuBarView mv = new MenuBarView(this);
		setJMenuBar(mv);

		setResizable(true);
		
//		try
//		{	// Set icon.
////			final URL url = ClassLoader.getSystemResource(IconCatalogue.CLIENT_ICON);
////			final Image icon = createImage((ImageProducer) url.getContent());
////			this.setIconImage(icon);
//
//		} catch (Exception e) {
//			String stackTrace = ExceptionUtils.getStackTrace(e);
//			Logging.writeToLog(true, e.toString()+" in Strat3GUI.ctor\nStackTrace:\n"+stackTrace, Logging.ERROR);
//		}

		formFactorConst = new ReadFile().readFileData();
		labels = extractLabels();
	}
	
	private ArrayList<String> extractLabels() {
		ArrayList<String> labels = new ArrayList<String>();
		
		for (OneConstantsRow str : formFactorConst) {
			labels.add(str.getLabel());
		}
		
		return labels;
	}
////////////////////////////////////////////////////////////////
	public ArrayList<OneConstantsRow> getFileData() {
		return formFactorConst;
	}

	public ArrayList<String> getLabels() {
		return labels;
	}

	public JDesktopPane getDesktop() {
		return desktop;
	}

	public Set<String> getUniqueAtoms() {
		for (Iterator<LayerComponent> it = layers.iterator(); it.hasNext();) {
			LayerComponent lc = (LayerComponent) it.next();
			for (Iterator<String> it2 = lc.getComponentAtomTypes().iterator(); it2.hasNext();) {
				String lad = (String) it2.next();
				uniqueAtoms.add(lad);
			}
		}
		
		return uniqueAtoms;
	}

//	public void setSelectedLabels(ArrayList<String> selectedLabels) {
//		this.selectedLabels = selectedLabels;
//	}

	public ArrayList<OneConstantsRow> getFormFactorConst() {
		return formFactorConst;
	}

	public Map<String, ArrayList<RStarAndF>> getfCurves() {
		return fCurves;
	}

	public void addfCurve(String atom, ArrayList<RStarAndF> curve) {
		this.fCurves.put(atom, curve);
	}

	public int getNumComponents() {
		return numComponents;
	}

	public void setNumComponents(int numc) {
		this.numComponents = numc;
	}

	public ArrayList<LayerComponent> getLayers() {
		return layers;
	}

	public void setLayers(ArrayList<LayerComponent> layers) {
		this.layers = layers;
	}

	public double getMaxRStar() {
		return maxRStar;
	}

	public void setMaxRStar(double maxRStar) {
		this.maxRStar = maxRStar;
	}

	public ArrayList<OneRStarSums> getSfSums() {
		return sfSums;
	}

	public void setSfSums(ArrayList<OneRStarSums> sfSums) {
		this.sfSums = sfSums;
	}

	public ArrayList<OneRStarSums> getLayerStructureFactorSums() {
		return layerStructureFactorSums;
	}

	public void setLayerStructureFactorSums(ArrayList<OneRStarSums> layerStructureFactorSums) {
		this.layerStructureFactorSums = layerStructureFactorSums;
	}

}
