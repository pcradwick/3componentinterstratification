package main;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.UIManager;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;

import utilities.Logging;

public class Strat3Main {
	static String osName = null;
	static Strat3GUI mainFrame;
	static String version = "1.0.0";
	
	public static void main(String[] args) {
		osName = System.getProperty("os.name");
		Logging.instance(Logging.CLIENT);

		Logging.writeToLog(true, "Start of Strat3Main", Logging.DEBUG);

//		setLookAndFeel();
		JFrame.setDefaultLookAndFeelDecorated(false);
		
		Strat3GUI mainGUI = new Strat3GUI();
		mainGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainGUI.setVisible(true);
		mainFrame = mainGUI;
		
		// Catch any uncaught exceptions so they are Logged
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			public void uncaughtException(Thread t, Throwable e) {
				String stackTrace = ExceptionUtils.getStackTrace(e);
				Logging.writeToLog(true, "Uncaught exception: "+e.toString()+"\nTrace: "+stackTrace, Logging.ERROR);
			}
		});

//		Logging.writeToLog(false, "End of Strat3Main.main.", Logging.DEBUG);

	}

	private static void setLookAndFeel()
	{
		if (osName.contains("Linux")) {		// Linux
//			String laf = UIManager.getSystemLookAndFeelClassName(); //best for linux? ????
			String laf = UIManager.getCrossPlatformLookAndFeelClassName();
			try {
				UIManager.setLookAndFeel(laf);
			} catch (Exception e) {
				String stackTrace = ExceptionUtils.getStackTrace(e);
				Logging.writeToLog(true, e.toString()+" in Main (linux).\nStackTrace:\n"+stackTrace, Logging.ERROR);
			}

		}
		else {//WINDOWS
			try {
				UIManager.setLookAndFeel(new PlasticXPLookAndFeel());
			} catch (Exception e) {
				String stackTrace = ExceptionUtils.getStackTrace(e);
				Logging.writeToLog(true, e.toString()+" in Main (windows).\nStackTrace:\n"+stackTrace, Logging.ERROR);
			}
		}

		try
		{	//make inactive text slightly darker
			UIManager.put("TextField.inactiveForeground", new Color(128,128,128));
			UIManager.put("FormattedTextField.inactiveForeground", new Color(128,128,128));
			UIManager.put("TextArea.inactiveForeground", new Color(128,128,128));
			UIManager.put("ComboBox.disabledForeground", new Color(128,128,128));
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.getStackTrace(e);
			Logging.writeToLog(true, e.toString()+" in Main (put).\nStackTrace:\n"+stackTrace, Logging.ERROR);
		}
	}

	public static Strat3GUI getMainFrame() {
		return mainFrame;
	}
	
	public static String getVersion() {
		return version;
	}
}
