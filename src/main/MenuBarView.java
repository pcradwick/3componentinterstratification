/*
 * Created on 12/12/2002
 * Copyright 2002 Tony Cradwick
 */
package main;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import diffpattern.DiffractionPanel;
import formfactors.FormFactorPanel;
import layerSF.CalculateLayerSF;
import layerSF.LayerComponent;
import utilities.Logging;


/**
 * @author tony
 * @version 1.0
 */
public class MenuBarView extends JMenuBar
{	
	private Frame frame = null;
	/**
	 * Instantiates a <code>MenuView</code> and as
	 * well as the default menu options an additional
	 * <code>javax.swing.JMenu</code> is provided.
	 */
	public MenuBarView(Strat3GUI f)
	{
		frame = f;

		JMenu menu = null;
		JMenuItem item1, item2, item3;
		item1=item2=item3=null;

		final MenuItemActionListener actionListener = new MenuItemActionListener();
		
		// File
		menu = new JMenu("File");
		item2 = new JMenuItem("Exit");
		item2.addActionListener(actionListener);

		item1 = new JMenuItem("Bug report");
		item1.addActionListener(actionListener);

		menu.add(item1);
		menu.add(item2);
		super.add(menu);

		// FormFactors
		menu = new JMenu("Layer Components");

		item1 = new JMenuItem("Select Atoms for Layer 1");
		item1.addActionListener(actionListener);
		menu.add(item1);

		item2 = new JMenuItem("Select Atoms for Layer 2");
		item2.addActionListener(actionListener);
		menu.add(item2);

		item3 = new JMenuItem("Select Atoms for Layer 3");
		item3.addActionListener(actionListener);
		menu.add(item3);

		add(menu);

		// LayerSF
		menu = new JMenu("Layer Structure Factors");
		item1 = new JMenuItem("Calculate Layer Structure Factors");
		item1.addActionListener(actionListener);
		menu.add(item1);
		
		item2 = new JMenuItem("Calculate LSF and Display");
		item2.addActionListener(actionListener);
		menu.add(item2);

		add(menu);

		// Diffraction Pattern
		menu = new JMenu("Diffraction Pattern");
		item1 = new JMenuItem("Calculate Diffraction Pattern");
		item1.addActionListener(actionListener);
		menu.add(item1);
		add(menu);

		// Help
		menu = new JMenu("Help");
		item1 = new JMenuItem("Contents...");
		item1.addActionListener(actionListener);
		menu.add(item1);

		menu.addSeparator();

		// about
		item1 = new JMenuItem("About");
		item1.addActionListener(actionListener);
		menu.add(item1);
		add(menu);


	}


	/****************************************************************************
	 *                      Action Listeners
	 ***************************************************************************/

	/** Listens for action events. */
	public class MenuItemActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent actionEvent) {
//			Logging.writeToLog(true, "MenuItemActionListener: actionEvent.getActionCommand() " + actionEvent.getActionCommand(), Logging.DEBUG);

			String title = "Select atom components of Layer ";
			String swStr = actionEvent.getActionCommand();
			
			switch(swStr) {
			case "Exit":
				frame.dispose();
				System.exit(0);
				break;

			case "Select Atoms for Layer 1":
				title += "1";
				LayerComponent layer1 = new LayerComponent(1);
				FormFactorPanel atomComponentsLayer1 = new FormFactorPanel(title, layer1);
				atomComponentsLayer1.setVisible(true);
				
				Strat3Main.getMainFrame().getDesktop().add(atomComponentsLayer1);
				Strat3Main.getMainFrame().setNumComponents(Strat3Main.getMainFrame().getNumComponents()+1);
				Strat3Main.getMainFrame().getLayers().add(layer1);
				break;

			case "Select Atoms for Layer 2":
				title += "2";
				LayerComponent layer2 = new LayerComponent(2);
				FormFactorPanel atomComponentsLayer2 = new FormFactorPanel(title, layer2);
				atomComponentsLayer2.setVisible(true);
				Strat3Main.getMainFrame().getDesktop().add(atomComponentsLayer2);
				atomComponentsLayer2.moveToFront();
				Strat3Main.getMainFrame().setNumComponents(Strat3Main.getMainFrame().getNumComponents()+1);
				Strat3Main.getMainFrame().getLayers().add(layer2);
				break;

			case "Select Atoms for Layer 3":
				if (Strat3Main.getMainFrame().getNumComponents() == 1) {
					JOptionPane.showMessageDialog(Strat3Main.getMainFrame(), "Define Layer 2 first", "Missing Layer 2",JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				title += "3";
				LayerComponent layer3 = new LayerComponent(3);
				FormFactorPanel atomComponentsLayer3 = new FormFactorPanel(title, layer3);
				atomComponentsLayer3.setVisible(true);
				Strat3Main.getMainFrame().getDesktop().add(atomComponentsLayer3);
				atomComponentsLayer3.moveToFront();
				Strat3Main.getMainFrame().setNumComponents(Strat3Main.getMainFrame().getNumComponents()+1);
				Strat3Main.getMainFrame().getLayers().add(layer3);
				break;

			case "Calculate Layer Structure Factors":
				new CalculateLayerSF(false);
				break;

			case "Calculate LSF and Display":
				new CalculateLayerSF(true);
				break;
				
			case "Calculate Diffraction Pattern":
				DiffractionPanel diffPattern = new DiffractionPanel("Calculation Data");
				Strat3Main.getMainFrame().getDesktop().add(diffPattern);
				diffPattern.moveToFront();
				break;

			case "Help":
				
				break;
				
			case "About":

				break;

			default:
				Logging.writeToLog(true, "Unknown Menu Command: " + actionEvent.getActionCommand(), Logging.ERROR);

			}
		}
	}


}
