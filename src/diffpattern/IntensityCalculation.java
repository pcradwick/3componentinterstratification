package diffpattern;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import layerSF.LayerComponent;
import layerSF.OneRStarSums;
import layerSF.CalculateLayerSF.CurvePlotter;
import main.Strat3Main;
import utilities.Logging;

public class IntensityCalculation {
//	private static int MAX_SEQUENCE;		// total no. of layers

	ArrayList<OneRStarSums> sfSums = Strat3Main.getMainFrame().getLayerStructureFactorSums();
	
	ArrayList<Combinations.LayerSequence> ABCCombinations;
	DiffractionData diffData;
	ArrayList<Output> diffractedIntensity = new ArrayList<Output>();
	
	int numLayers;
	int numComponents = Strat3Main.getMainFrame().getNumComponents(); 
	String title = null;
	
	public IntensityCalculation(DiffractionData dd, ArrayList<Combinations.LayerSequence> comb) {
		diffData = dd;

		numLayers = diffData.getNumLayers();

		ABCCombinations = comb;

		ArrayList<LayerComponent> layers = Strat3Main.getMainFrame().getLayers();


		title = numLayers+" Layer Intensity calculation for "+
				layers.get(0).getName()+"("+diffData.getpA()+") ";
		
		if (numComponents == 2) {
			title += layers.get(1).getName()+"("+diffData.getpB()+") ";
		}
		if (numComponents == 3) {
			title += layers.get(1).getName()+"("+diffData.getpB()+") ";
			title += layers.get(2).getName()+"("+diffData.getpC()+")";
		}

	}

	/*
	 * Use the LayerSequence data in ABCCombinations & DiffractionData
	 * to calculate the diffraction pattern
	 * 
	 * Note: minimum r* value is 0.02 not 0
	 */
	public void calculateIntensity() {
		double pts = (diffData.getEndRStar() - diffData.getStartRStar()) / diffData.getDeltaRStar();
		int numPts = (int) Double.parseDouble(""+pts);
		double [] interSF = new double [15];
		
		double delta = diffData.getDeltaRStar();
		double rStar = diffData.getStartRStar() - delta;
		double arg, ds, denom, lpFactor, intensity, twoPi = 2.0 * Math.PI;
		double backStop = diffData.getNumLayers() * (1.0 - diffData.getBackStopFactor());
		double theta, twoTheta, sinTheta, cosTheta, cos2Theta, sinSqTheta, cosSqTheta;
		double radToDegrees = 180.0 / Math.PI;
		
		for (int k=0; k<numPts; ++k) {
			rStar += delta;
			sinTheta = rStar * diffData.getLlambda() * 0.5;
			ds = 1.0 / rStar;
			intensity = 0.0;
			interSF = interpolateLayerStructureFactorTables(rStar);
			
			// calculate LP Factor
			sinSqTheta = sinTheta * sinTheta;
			cosSqTheta = 1.0 - sinSqTheta;
			cosTheta = Math.sqrt(cosSqTheta);
			theta = Math.atan(sinTheta/cosTheta) * radToDegrees;
			twoTheta = 2.0 * theta;
			cos2Theta = cosSqTheta - sinSqTheta;
			if (diffData.isRandomLP()) {
				denom = sinSqTheta * cosTheta;
			} else {
				denom = 2.0 * sinTheta * cosTheta;
			}
			lpFactor = (1.0 + cos2Theta * cos2Theta) / denom;
			
			/*
			 * There are three types of terms contributing to the final intensity
			 * 	1. Initial intensity terms pA|vA|**2, pB.|vB|**2, pC.|vC|**2
			 * 	2. pAA.vAA, pBB.vBB, pCC.vBB	(vAA = |vA|**2 etc...)
			 * 	3. Terms for each mixed combination in ABCCombinations
			 * 		i.e. A-B, A-C, B-A, B-C,C-A, C-B
			 * 		These cross terms have both cos and sin terms.
			 * 		
			 */
			arg = twoPi * rStar;
			intensity += backStop * (interSF[0]*diffData.getpA() +
										interSF[1]*diffData.getpB() +
										interSF[2]*diffData.getpC());
			
			// Sum intensity terms for each combination in ABCCombinations
			for (Iterator it = ABCCombinations.iterator(); it.hasNext();) {
				Combinations.LayerSequence lseq = (Combinations.LayerSequence) it.next();
				String ends = lseq.getComID().getEndLayers();
				double prob = lseq.getProbabilityProduct();
				double spac = lseq.getPhaseTermR();
				double coeff = lseq.getCoeff();
				
				switch (ends) {
				case "AA":
					intensity += interSF[0]*prob*Math.cos(arg*spac)*coeff;
					break;
					
				case "BB":
					intensity += interSF[1]*prob*Math.cos(arg*spac)*coeff;
					break;
					
				case "CC":
					intensity += interSF[2]*prob*Math.cos(arg*spac)*coeff;
					break;
					
				case "AB":
					intensity += interSF[3]*prob*Math.cos(arg*spac)*coeff;
					intensity += interSF[4]*prob*Math.sin(arg*spac)*coeff;
					break;
					
				case "AC":
					intensity += interSF[5]*prob*Math.cos(arg*spac)*coeff;
					intensity += interSF[6]*prob*Math.sin(arg*spac)*coeff;
					break;
					
				case "BA":
					intensity += interSF[7]*prob*Math.cos(arg*spac)*coeff;
					intensity -= interSF[8]*prob*Math.sin(arg*spac)*coeff;
					break;
					
				case "BC":
					intensity += interSF[9]*prob*Math.cos(arg*spac)*coeff;
					intensity += interSF[10]*prob*Math.sin(arg*spac)*coeff;
					break;
					
				case "CA":
					intensity += interSF[11]*prob*Math.cos(arg*spac)*coeff;
					intensity -= interSF[12]*prob*Math.sin(arg*spac)*coeff;
					break;
					
				case "CB":
					intensity += interSF[13]*prob*Math.cos(arg*spac)*coeff;
					intensity -= interSF[14]*prob*Math.sin(arg*spac)*coeff;
					break;
					
				}
			}	
			
			/*
			 *  Not important for diagnostic purposes Brown p. 409
			 *  as positions of peaks are of more diagnostic value
			 *  and this term tends to suppress detail at low 
			 *  theta due to the denominator.
			 */
			lpFactor = 1.0;		
					
			double cor = intensity * lpFactor;
			Output out =  new Output(rStar, twoTheta, ds, cor);
			diffractedIntensity.add(out);
			
		}		// End of r* npts loop
		
		// Apply normalisation factor if 
		
		plotIntensityCurve();
	}

	/*
	 * Interpolate Layer Structure Factor curves in sfSums at given r*
	 * 
	 * 	sfSums is storage for the end layer sums for one r* value:
	 * 		sumCosAA, sumCosBB, sumCosCC,
	 * 		sumCosAB, sumSinAB, sumCosAC, sumSinAC,
	 * 		sumCosBA, sumSinBA, sumCosBC, sumSinBC
	 * 		sumCosCA, sumSinCA, sumCosCB, sumSinCB
	 * 
	 * 	result is storage for the interpolated values of the above sums
	 * 		sumCosAA[0], 	sumCosBB[1],	sumCosCC[2], 
	 * 		sumCosAB[3],	sumSinAB[4],	sumCosAC[5],	sumSinAC[6], 
	 * 		sumCosBA[7],	sumSinBA[8],	sumCosBC[9],	sumSinBC[10],
	 * 		sumCosCA[11],	sumSinCA[12],	sumCosCB[13],	sumSinCB[14]
	 * 
	 */

	private double [] interpolateLayerStructureFactorTables(double rStar) {
		double [] result = new double [15];
		OneRStarSums lastRStar=null, nextRStar=null;
		
		boolean found = false;
		for (Iterator<OneRStarSums> it = sfSums.iterator(); it.hasNext();) {
			OneRStarSums rSum = (OneRStarSums) it.next();
			nextRStar = rSum;
			if (nextRStar.getrStar() > rStar) {
				found = true;
				break;
			}
			
			lastRStar = rSum;
		}
		
		if (!found) {
			String msg = "Requested r* = "+rStar+" is greater than calculated Layer Structure Factor Sums";
			JOptionPane.showMessageDialog(Strat3Main.getMainFrame(), msg, "ERROR", JOptionPane.ERROR_MESSAGE);
			Logging.writeToLog(true, msg, Logging.ERROR);
			return null;
		}
		
		double delRStar = nextRStar.getrStar() - lastRStar.getrStar();
		double factor = (rStar - lastRStar.getrStar()) / delRStar;

		double [] minSF = lastRStar.getSums();
		double [] maxSF = nextRStar.getSums();

		result[0] = maxSF[0]*factor + minSF[0]*(1.0-factor);
		result[1] = maxSF[1]*factor + minSF[1]*(1.0-factor);
		result[2] = maxSF[2]*factor + minSF[2]*(1.0-factor);
		result[3] = maxSF[3]*factor + minSF[3]*(1.0-factor);
		result[4] = maxSF[4]*factor + minSF[4]*(1.0-factor);
		result[5] = maxSF[5]*factor + minSF[5]*(1.0-factor);
		result[6] = maxSF[6]*factor + minSF[6]*(1.0-factor);
		result[7] = maxSF[7]*factor + minSF[7]*(1.0-factor);
		result[8] = maxSF[8]*factor + minSF[8]*(1.0-factor);
		result[9] = maxSF[8]*factor + minSF[9]*(1.0-factor);
		result[10] = maxSF[10]*factor + minSF[10]*(1.0-factor);
		result[11] = maxSF[11]*factor + minSF[11]*(1.0-factor);
		result[12] = maxSF[12]*factor + minSF[12]*(1.0-factor);
		result[13] = maxSF[13]*factor + minSF[13]*(1.0-factor);
		result[14] = maxSF[14]*factor + minSF[14]*(1.0-factor);

		return result;
	}

	/*
	 * The calculated intensity values are in the array diffractedIntensity
	 * within the Output objects. Use intensity and r*
	 */
	public void plotIntensityCurve() {
		
		XYSeries series = new XYSeries("");
		for (Iterator<Output> it = diffractedIntensity.iterator(); it.hasNext();) {
			Output oup = (Output) it.next();
			series.add(oup.getrStar(), oup.getIntensity());
		}

		CurvePlotter cp = new CurvePlotter(title, series);
		cp.pack();
		cp.setVisible(true);

	}

	//////////////////////////////////////////////////////////////////////
	//					INNER CLASSES
	//////////////////////////////////////////////////////////////////////

	class Output {
		double rStar, twoTheta, dSpacing, intensity;
		
		Output(double r, double t, double d, double i) {
			this.rStar = r;
			this.twoTheta = t;
			this.dSpacing = d;
			this.intensity = i;
		}

		public double getrStar() {
			return rStar;
		}

		public void setrStar(double rStar) {
			this.rStar = rStar;
		}

		public double getTwoTheta() {
			return twoTheta;
		}

		public void setTwoTheta(double twoTheta) {
			this.twoTheta = twoTheta;
		}

		public double getdSpacing() {
			return dSpacing;
		}

		public void setdSpacing(double dSpacing) {
			this.dSpacing = dSpacing;
		}

		public double getIntensity() {
			return intensity;
		}

		public void setIntensity(double intensity) {
			this.intensity = intensity;
		}
		
	}
	
	public class CurvePlotter extends ApplicationFrame {
		
		public CurvePlotter(String title, XYSeries series) {
			super(title);
			
			XYSeriesCollection data = new XYSeriesCollection(series);
			JFreeChart chart = ChartFactory.createXYLineChart(
					title,
					"r*",
					"I(r*)",
					data,
					PlotOrientation.VERTICAL,
					false,
					false,
					false
					);
			
			ChartPanel chartPanel = new ChartPanel(chart);
			chartPanel.setPreferredSize(new Dimension(500, 200)); 
			setContentPane(chartPanel);
		}
		
	}
	
	
	


}
