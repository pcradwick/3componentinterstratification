package diffpattern;

import java.util.ArrayList;
import java.util.Iterator;

class Combinations {
	private static int MAX_SEQUENCE;		// total no. of layers
	ArrayList<LayerSequence> ABCCombinations = new ArrayList<LayerSequence>();
	int TotalNumLayers;
	DiffractionData diffData;
	
	/*
	 * nA = no. layers of type A in the sequence 
	 * nB = no. layers of type B in the sequence 
	 * nC = no. layers of type C in the sequence 
	 * ns = count of no. layers in the sequence 
	 */
	Combinations(int numLayers, DiffractionData dd) {
		this.TotalNumLayers = numLayers;
		Combinations.MAX_SEQUENCE = TotalNumLayers;
		this.diffData = dd;
	}
	
	/*
	 * For all possible values for nA, nB, nC where 
	 * 		nA+nB+nC = nu and nu takes all values from 2 -> N
	 * 
	 * 		N = total no. of layers
	 * 		nA = no. of layers of type A
	 * 		nB = no. of layers of type B
	 * 		nC = no. of layers of type C
	 * 
	 */
	
	public void createCombinations(int numLayers) {
		ArrayList<int []> layers;
		
		for (int nu=2; nu<=numLayers; ++nu) {		// nu = nA + nB + nC
			switch(nu) {
			case 2:
				layers = twoLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 3:
				layers = threeLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 4:
				layers = fourLayerSequences();
				addToABCCombinations(layers, nu);
				break; 

			case 5:
				layers = fiveLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 6:
				layers = sixLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 7:
				layers = sevenLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 8:
				layers = eightLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 9:
				layers = nineLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 10:
				layers = tenLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 11:
				layers = elevenLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 12:
				layers = twelveLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 13:
				layers = thirteenLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 14:
				layers = fourteenLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			case 15:
				layers = fifteenLayerSequences();
				addToABCCombinations(layers, nu);
				break;

			}
		}			// End of sequence nu loop
	}

	private void addToABCCombinations(ArrayList<int []> layers, int ns) {
		layerLoop:for (Iterator<int[]> it1 = layers.iterator(); it1.hasNext();) {
			int[] is = (int[]) it1.next();
			LayerSequence lc = new LayerSequence(is, ns);
		
//			for (Iterator<LayerSequence> it2 = ABCCombinations.iterator(); it2.hasNext();) {
//				LayerSequence stored= (LayerSequence) it2.next();
//				
//				if (lc.equals(stored) && diffData.isNoDuplicateCombinations()) {
//					stored.factor++;
//					continue layerLoop;					// Don't store if already in ABCCombinations
//				}
//				
//			}
			
			lc.setCoeff(TotalNumLayers - ns +1);		// weighting factor
			ABCCombinations.add(lc);					// Add it if not
		}
	}

	private ArrayList<int []> twoLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> twolayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				oneSequence = new int [MAX_SEQUENCE];
				oneSequence[0] = l1;
				oneSequence[1] = l2;
				twolayers.add(oneSequence);
			}
		}

		return twolayers;
	}

	private ArrayList<int []> threeLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> threelayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					oneSequence = new int [MAX_SEQUENCE];
					oneSequence[0] = l1;
					oneSequence[1] = l2;
					oneSequence[2] = l3;
					threelayers.add(oneSequence);
				}
			}
		}

		return threelayers;
	}

	private ArrayList<int []> fourLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> fourlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						oneSequence = new int [MAX_SEQUENCE];
						oneSequence[0] = l1;
						oneSequence[1] = l2;
						oneSequence[2] = l3;
						oneSequence[3] = l4;
						fourlayers.add(oneSequence);
					}
				}
			}
		}

		return fourlayers;
	}

	private ArrayList<int []> fiveLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> fivelayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							oneSequence = new int [MAX_SEQUENCE];
							oneSequence[0] = l1;
							oneSequence[1] = l2;
							oneSequence[2] = l3;
							oneSequence[3] = l4;
							oneSequence[4] = l5;
							fivelayers.add(oneSequence);
						}
					}
				}
			}
		}

		return fivelayers;
	}
	private ArrayList<int []> sixLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> sixlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								oneSequence = new int [MAX_SEQUENCE];
								oneSequence[0] = l1;
								oneSequence[1] = l2;
								oneSequence[2] = l3;
								oneSequence[3] = l4;
								oneSequence[4] = l5;
								oneSequence[5] = l6;
								sixlayers.add(oneSequence);
							}
						}
					}
				}
			}
		}

		return sixlayers;
	}

	private ArrayList<int []> sevenLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> sevenlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									oneSequence = new int [MAX_SEQUENCE];
									oneSequence[0] = l1;
									oneSequence[1] = l2;
									oneSequence[2] = l3;
									oneSequence[3] = l4;
									oneSequence[4] = l5;
									oneSequence[5] = l6;
									oneSequence[6] = l7;
									sevenlayers.add(oneSequence);
								}
							}
						}
					}
				}
			}
		}

		return sevenlayers;
	}

	private ArrayList<int []> eightLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> eightlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									for(int l8=0; l8<3; ++l8) {
										oneSequence = new int [MAX_SEQUENCE];
										oneSequence[0] = l1;
										oneSequence[1] = l2;
										oneSequence[2] = l3;
										oneSequence[3] = l4;
										oneSequence[4] = l5;
										oneSequence[5] = l6;
										oneSequence[6] = l7;
										oneSequence[7] = l8;
										eightlayers.add(oneSequence);
									}
								}
							}
						}
					}
				}
			}
		}

		return eightlayers;
	}

	private ArrayList<int []> nineLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> ninelayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									for(int l8=0; l8<3; ++l8) {
										for(int l9=0; l9<3; ++l9) {
											oneSequence = new int [MAX_SEQUENCE];
											oneSequence[0] = l1;
											oneSequence[1] = l2;
											oneSequence[2] = l3;
											oneSequence[3] = l4;
											oneSequence[4] = l5;
											oneSequence[5] = l6;
											oneSequence[6] = l7;
											oneSequence[7] = l8;
											oneSequence[8] = l9;
											ninelayers.add(oneSequence);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return ninelayers;
	}


	private ArrayList<int []> tenLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> tenlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									for(int l8=0; l8<3; ++l8) {
										for(int l9=0; l9<3; ++l9) {
											for(int l10=0; l10<3; ++l10) {
												oneSequence = new int [MAX_SEQUENCE];
												oneSequence[0] = l1;
												oneSequence[1] = l2;
												oneSequence[2] = l3;
												oneSequence[3] = l4;
												oneSequence[4] = l5;
												oneSequence[5] = l6;
												oneSequence[6] = l7;
												oneSequence[7] = l8;
												oneSequence[8] = l9;
												oneSequence[9] = l10;
												tenlayers.add(oneSequence);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return tenlayers;
	}

	private ArrayList<int []> elevenLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> elevenlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									for(int l8=0; l8<3; ++l8) {
										for(int l9=0; l9<3; ++l9) {
											for(int l10=0; l10<3; ++l10) {
												for(int l11=0; l11<3; ++l11) {
													oneSequence = new int [MAX_SEQUENCE];
													oneSequence[0] = l1;
													oneSequence[1] = l2;
													oneSequence[2] = l3;
													oneSequence[3] = l4;
													oneSequence[4] = l5;
													oneSequence[5] = l6;
													oneSequence[6] = l7;
													oneSequence[7] = l8;
													oneSequence[8] = l9;
													oneSequence[9] = l10;
													oneSequence[10] = l11;
													elevenlayers.add(oneSequence);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return elevenlayers;
	}

	private ArrayList<int []> twelveLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> twelvelayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									for(int l8=0; l8<3; ++l8) {
										for(int l9=0; l9<3; ++l9) {
											for(int l10=0; l10<3; ++l10) {
												for(int l11=0; l11<3; ++l11) {
													for(int l12=0; l12<3; ++l12) {
														oneSequence = new int [MAX_SEQUENCE];
														oneSequence[0] = l1;
														oneSequence[1] = l2;
														oneSequence[2] = l3;
														oneSequence[3] = l4;
														oneSequence[4] = l5;
														oneSequence[5] = l6;
														oneSequence[6] = l7;
														oneSequence[7] = l8;
														oneSequence[8] = l9;
														oneSequence[9] = l10;
														oneSequence[10] = l11;
														oneSequence[11] = l12;
														twelvelayers.add(oneSequence);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return twelvelayers;
	}

	private ArrayList<int []> thirteenLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> thirteenlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									for(int l8=0; l8<3; ++l8) {
										for(int l9=0; l9<3; ++l9) {
											for(int l10=0; l10<3; ++l10) {
												for(int l11=0; l11<3; ++l11) {
													for(int l12=0; l12<3; ++l12) {
														for(int l13=0; l13<3; ++l13) {
															oneSequence = new int [MAX_SEQUENCE];
															oneSequence[0] = l1;
															oneSequence[1] = l2;
															oneSequence[2] = l3;
															oneSequence[3] = l4;
															oneSequence[4] = l5;
															oneSequence[5] = l6;
															oneSequence[6] = l7;
															oneSequence[7] = l8;
															oneSequence[8] = l9;
															oneSequence[9] = l10;
															oneSequence[10] = l11;
															oneSequence[11] = l12;
															oneSequence[12] = l13;
															thirteenlayers.add(oneSequence);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return thirteenlayers;
	}

	private ArrayList<int []> fourteenLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> fourteenlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									for(int l8=0; l8<3; ++l8) {
										for(int l9=0; l9<3; ++l9) {
											for(int l10=0; l10<3; ++l10) {
												for(int l11=0; l11<3; ++l11) {
													for(int l12=0; l12<3; ++l12) {
														for(int l13=0; l13<3; ++l13) {
															for(int l14=0; l14<3; ++l14) {
																oneSequence = new int [MAX_SEQUENCE];
																oneSequence[0] = l1;
																oneSequence[1] = l2;
																oneSequence[2] = l3;
																oneSequence[3] = l4;
																oneSequence[4] = l5;
																oneSequence[5] = l6;
																oneSequence[6] = l7;
																oneSequence[7] = l8;
																oneSequence[8] = l9;
																oneSequence[9] = l10;
																oneSequence[10] = l11;
																oneSequence[11] = l12;
																oneSequence[12] = l13;
																oneSequence[13] = l14;
																fourteenlayers.add(oneSequence);
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return fourteenlayers;
	}

	private ArrayList<int []> fifteenLayerSequences() {
		int [] oneSequence;
		ArrayList<int []> fifteenlayers = new ArrayList<int []>();

		for (int l1=0; l1<3; ++l1) {
			for (int l2=0; l2<3; ++l2) {
				for (int l3=0; l3<3; ++l3) {
					for(int l4=0; l4<3; ++l4) {
						for(int l5=0; l5<3; ++l5) {
							for(int l6=0; l6<3; ++l6) {
								for(int l7=0; l7<3; ++l7) {
									for(int l8=0; l8<3; ++l8) {
										for(int l9=0; l9<3; ++l9) {
											for(int l10=0; l10<3; ++l10) {
												for(int l11=0; l11<3; ++l11) {
													for(int l12=0; l12<3; ++l12) {
														for(int l13=0; l13<3; ++l13) {
															for(int l14=0; l14<3; ++l14) {
																for(int l15=0; l15<3; ++l15) {
																	oneSequence = new int [MAX_SEQUENCE];
																	oneSequence[0] = l1;
																	oneSequence[1] = l2;
																	oneSequence[2] = l3;
																	oneSequence[3] = l4;
																	oneSequence[4] = l5;
																	oneSequence[5] = l6;
																	oneSequence[6] = l7;
																	oneSequence[7] = l8;
																	oneSequence[8] = l9;
																	oneSequence[9] = l10;
																	oneSequence[10] = l11;
																	oneSequence[11] = l12;
																	oneSequence[12] = l13;
																	oneSequence[13] = l14;
																	oneSequence[14] = l15;
																	fifteenlayers.add(oneSequence);
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return fifteenlayers;
	}

	public ArrayList<LayerSequence> getABCCombinations() {
		return ABCCombinations;
	}

	public void setABCCombinations(ArrayList<LayerSequence> aBCCombinations) {
		ABCCombinations = aBCCombinations;
	}
	
	


	////////////////////////////////////////////////////////////////////
	//					INNER CLASSES
	////////////////////////////////////////////////////////////////////
	/*
	 * Storage for a single A-B-C combination
	 */
	class LayerSequence {
		private String [] layerSymbols = {"A", "B", "C"};
		private int nA = 0, nB = 0, nC = 0;
		private String ABC;
		private String ends;
		private int sequenceLength = 0; 
		private double probabilityProduct = 0.0;
		private double phaseTermR = 0.0;
		private double coeff = 1.0;
		private CombinationIdentifier comID = null;
		private double factor = 1;			// Counts 'duplicates' if noDuplicateCombinations is true

		LayerSequence(int [] aSequence, int numLayersInSequence) {
			setSequenceLength(aSequence.length);
			this.nA = 0;
			this.nB = 0;
			this.nC = 0;
			
			/*
			 *  Count the components
			 *  and convert to "ABC"
			 */
			ABC = "";
			for (int k=0; k<numLayersInSequence; ++k) {
				ABC += layerSymbols[aSequence[k]];
				switch(aSequence[k]) {
				case 0:
					nA += 1;
					break;

				case 1:
					nB += 1;
					break;

				case 2:
					nC += 1;
					break;
				}
			}
			
			char [] chs = ABC.toCharArray();
			char start = chs[0];
			char end = chs[ABC.length()-1];
			ends = String.valueOf(start) + String.valueOf(end);
			int midA=nA, midB=nB, midC=nC;		
			
			switch(start) {
			case 'A':
				midA -= 1;
				break;
				
			case 'B':
				midB -= 1;
				break;
				
			case 'C':
				midC -= 1;
				break;
			}
			
			switch(end) {
			case 'A':
				midA -= 1;
				break;
				
			case 'B':
				midB -= 1;
				break;
				
			case 'C':
				midC -= 1;
				break;
			}
			
			comID = new CombinationIdentifier(ends, midA, midB, midC);


		}

		public boolean equals(LayerSequence lays) {
			return getComID().equals(lays.getComID());
		}
		
		public double getProbabilityProduct() {
			return probabilityProduct;
		}

		public void setProbabilityProduct(double probabilityProduct) {
			this.probabilityProduct = probabilityProduct;
		}

		public double getPhaseTermR() {
			return phaseTermR;
		}

		public void setPhaseTermR(double phaseTermR) {
			this.phaseTermR = phaseTermR;
		}

		public String getABC() {
			return ABC;
		}

		public int getSequenceLength() {
			return sequenceLength;
		}

		public void setSequenceLength(int sequenceLength) {
			this.sequenceLength = sequenceLength;
		}

		public int getnA() {
			return nA;
		}

		public int getnB() {
			return nB;
		}

		public int getnC() {
			return nC;
		}

		public CombinationIdentifier getComID() {
			return comID;
		}

		public void setComID(CombinationIdentifier comID) {
			this.comID = comID;
		}

		public double getCoeff() {
			return coeff;
		}

		public void setCoeff(double coeff) {
			this.coeff = coeff;
		}

	}
	
	/*
	 * Each A-B-C combination has a unique identifier based on the
	 * type of the end layers a, b or C and the number of each type
	 * in the intervening (middle) layers (excluding ends)
	 */
	class CombinationIdentifier {
		private String endLayers = "";
		private int numMidA = 0, numMidB = 0, numMidC = 0;
		
		CombinationIdentifier(String el, int nA, int nB, int nC) {
			this.endLayers = el;
			this.numMidA = nA;
			this.numMidB = nB;
			this.numMidC = nC;
		}
		
		public boolean equals(CombinationIdentifier o1) {
			String ends1 = o1.getEndLayers();
			int nA1 = o1.getNumMidA();
			int nB1 = o1.getNumMidB();
			int nC1 = o1.getNumMidC();
			
			return endLayers.equals(ends1) && 
					numMidA == nA1 && numMidB == nB1 && numMidC == nC1;

		}

		public String getEndLayers() {
			return endLayers;
		}

		public void setEndLayers(String endLayers) {
			this.endLayers = endLayers;
		}

		public int getNumMidA() {
			return numMidA;
		}

		public void setNumMidA(int numMidA) {
			this.numMidA = numMidA;
		}

		public int getNumMidB() {
			return numMidB;
		}

		public void setNumMidB(int numMidB) {
			this.numMidB = numMidB;
		}

		public int getNumMidC() {
			return numMidC;
		}

		public void setNumMidC(int numMidC) {
			this.numMidC = numMidC;
		}
	}

}
