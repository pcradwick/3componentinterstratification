package diffpattern;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import utilities.SystemSettings;

public class Probabilities extends JInternalFrame {

	Probabilities thisPane;
	DiffractionData diffData;
	
	// icons
	String slash = SystemSettings.instance().getFileSeparator();
	String imageDir = SystemSettings.instance().getWorkingDir()+
								slash+"resources"+slash+"images"+slash+"icons";
	Icon go  = new ImageIcon(imageDir+slash+"icons8-play-24.png");

	// GUI components
	JLabel paLabel = new JLabel("PA");
	JTextField paValue = new JTextField("", 5);
	JLabel pbLabel = new JLabel("PB");
	JTextField pbValue = new JTextField("", 5);
	JLabel pcLabel = new JLabel("PC");
	JTextField pcValue = new JTextField("", 5);
	JLabel paaLabel = new JLabel("PAA");
	JTextField paaValue = new JTextField("", 5);
	JLabel pbbLabel = new JLabel("PBB");
	JTextField pbbValue = new JTextField("", 5);
	JLabel pccLabel = new JLabel("PCC");
	JTextField pccValue = new JTextField("", 5);
	JLabel pabLabel = new JLabel("PAB");
	JTextField pabValue = new JTextField("", 5);
	JLabel pbaLabel = new JLabel("PBA");
	JTextField pbaValue = new JTextField("", 5);
	JLabel pacLabel = new JLabel("PAC");
	JTextField pacValue = new JTextField("", 5);
	JLabel pcaLabel = new JLabel("PCA");
	JTextField pcaValue = new JTextField("", 5);
	JLabel pbcLabel = new JLabel("PBC");
	JTextField pbcValue = new JTextField("", 5);
	JLabel pcbLabel = new JLabel("PCB");
	JTextField pcbValue = new JTextField("", 5);

	JButton calculate = new JButton("Calculate", go);

	static final int xOffset = 250, yOffset = 50;
	
	double pA = 0.0, pB = 0.0, pC = 0.0;
	double pAA = 0.0, pBB = 0.0, pCC = 0.0;
	double pAB = 0.0, pBA = 0.0, pAC = 0.0, pCA = 0.0, pBC = 0.0, pCB = 0.0;
			

	public Probabilities(String title, DiffractionData dd) {
		super(title);

		diffData = dd;
		thisPane = this;
		initialiseLayout();
		
	    setSize(new Dimension(300, 500));
	    
	    setLocation(xOffset, yOffset);
		
		setVisible(true);
	}

	private void initialiseLayout() {
		JPanel mainPane = new JPanel();
		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
		mainPane.setPreferredSize(new Dimension(300, 450));

		JPanel quantity = new JPanel();
		quantity.setBorder(BorderFactory.createTitledBorder("Component proportions     \u03a3 = 1.0"));
		quantity.setLayout(new BoxLayout(quantity, BoxLayout.Y_AXIS));

		JPanel paPanel = new JPanel();
		paPanel.setLayout(new FlowLayout());
		paPanel.setPreferredSize(new Dimension(200, 100));
		paPanel.add(paLabel);
		paPanel.add(paValue);

		JPanel pbPanel = new JPanel();
		pbPanel.setLayout(new FlowLayout());
		pbPanel.setPreferredSize(new Dimension(200, 100));
		pbPanel.add(pbLabel);
		pbPanel.add(pbValue);

		JPanel pcPanel = new JPanel();
		pcPanel.setLayout(new FlowLayout());
		pcPanel.setPreferredSize(new Dimension(200, 100));
		pcPanel.add(pcLabel);
		pcPanel.add(pcValue);

		quantity.add(paPanel);
		quantity.add(pbPanel);
		quantity.add(pcPanel);
		
		JPanel probabilities = new JPanel();
		probabilities.setBorder(BorderFactory.createTitledBorder("End Layer Probabilities"));
		probabilities.setLayout(new BoxLayout(probabilities, BoxLayout.Y_AXIS));
		
		JPanel paaPanel = new JPanel();
		paaPanel.setLayout(new FlowLayout());
		paaPanel.setPreferredSize(new Dimension(200, 100));
		paaPanel.add(paaLabel);
		paaPanel.add(paaValue);

		JPanel pbbPanel = new JPanel();
		pbbPanel.setLayout(new FlowLayout());
		pbbPanel.setPreferredSize(new Dimension(200, 100));
		pbbPanel.add(pbbLabel);
		pbbPanel.add(pbbValue);

		JPanel pccPanel = new JPanel();
		pccPanel.setLayout(new FlowLayout());
		pccPanel.setPreferredSize(new Dimension(200, 100));
		pccPanel.add(pccLabel);
		pccPanel.add(pccValue);

		JPanel pabPanel = new JPanel();
		pabPanel.setLayout(new FlowLayout());
		pabPanel.setPreferredSize(new Dimension(200, 100));
		pabPanel.add(pabLabel);
		pabPanel.add(pabValue);

		JPanel pbaPanel = new JPanel();
		pbaPanel.setLayout(new FlowLayout());
		pbaPanel.setPreferredSize(new Dimension(200, 100));
		pbaPanel.add(pbaLabel);
		pbaPanel.add(pbaValue);

		JPanel pacPanel = new JPanel();
		pacPanel.setLayout(new FlowLayout());
		pacPanel.setPreferredSize(new Dimension(200, 100));
		pacPanel.add(pacLabel);
		pacPanel.add(pacValue);

		JPanel pcaPanel = new JPanel();
		pcaPanel.setLayout(new FlowLayout());
		pcaPanel.setPreferredSize(new Dimension(200, 100));
		pcaPanel.add(pcaLabel);
		pcaPanel.add(pcaValue);

		JPanel pbcPanel = new JPanel();
		pbcPanel.setLayout(new FlowLayout());
		pbcPanel.setPreferredSize(new Dimension(200, 100));
		pbcPanel.add(pbcLabel);
		pbcPanel.add(pbcValue);

		JPanel pcbPanel = new JPanel();
		pcbPanel.setLayout(new FlowLayout());
		pcbPanel.setPreferredSize(new Dimension(200, 100));
		pcbPanel.add(pcbLabel);
		pcbPanel.add(pcbValue);

		probabilities.add(paaPanel);
		probabilities.add(paaPanel);
		probabilities.add(pbbPanel);
		probabilities.add(pccPanel);
		probabilities.add(pabPanel);
		probabilities.add(pbaPanel);
		probabilities.add(pacPanel);
		probabilities.add(pcaPanel);
		probabilities.add(pbcPanel);
		probabilities.add(pcbPanel);
		
		JPanel buttonPanel = new JPanel();
		calculate.addActionListener(new buttonListener());
		buttonPanel.add(calculate);

		mainPane.add(quantity);
		mainPane.add(probabilities);
		mainPane.add(buttonPanel);
		
		this.setContentPane(mainPane);
	}

	/* Calculate the probabilities of all possible sequences within
	 * the defined total number of layers to be used in the calculations.
	 * If there are 5 layers i.e. N = 5 and 3 possible component layers A, B, C
	 * and nA, nB, nC are the numbers of A, B, C within  the total N.
	 * There will be 2, 3, 4, 5 layer sequences and nA, nB, nC can take all
	 * possible values from 0..N with the restriction: nA + nB + nC = N
	 * For example:		Using the total no. of layers, N = 5
	 * Possible values for nA, nB, nC are:
	 * 
	 * nA		0			 1			    2			  3			4		5
	 * nB  5 4  3  2 1 0  4 3  2 1 0    3  2  1  0      2 1  0     1 0      0
	 * nC  0 1  2  3 4 5  0 1  2 3 4    0  1  2  3      0 1  2     0 1      0
	 *     
	 * In this example the total number of combinations of A, B, C = 360
	 * 
	 * All data input on the Main & Probabilities Dialogs is now in diffData
	 * The calculated probability and phase for this combination are stored
	 * in the Combinations.LayerSequence containing the input data.
	 */
	
	public void calculateProbabilities(ArrayList<Combinations.LayerSequence> ABCCombinations) {
		int index = ABCCombinations.size() - 1;
		String lastCombination = ABCCombinations.get(index).getABC();
		int numLayers = lastCombination.length();
		
		for (Iterator<Combinations.LayerSequence> it = ABCCombinations.iterator(); it.hasNext();) {
			Combinations.LayerSequence lseq = (Combinations.LayerSequence) it.next();
			String seq = lseq.getABC();
			char [] chs = seq.toCharArray();
			double probability = lseq.getProbabilityProduct();
			double phase = lseq.getPhaseTermR();
			double coeff = 1.0;
			
			for (int k=0; k<seq.length(); ++k) {
				if (k == 0) {
					switch(chs[0]) {
					case 'A':
						probability = diffData.getpA();
						break;
						
					case 'B':
						probability = diffData.getpB();
						break;
						
					case 'C':
						probability = diffData.getpC();
						break;
					}
				} else {		// k > 0. Check chs[k-1] & chs[k]
					String twoCharSeq = Character.toString(chs[k-1]) + Character.toString(chs[k]);
					switch(twoCharSeq) {
					case "AA":
						probability *= diffData.getpAA();
						break;
						
					case "AB":
						probability *= diffData.getpAB();
						break;
						
					case "AC":
						probability *= diffData.getpAC();
						break;
						
					case "BA":
						probability *= diffData.getpBA();
						break;
						
					case "BB":
						probability *= diffData.getpBB();
						break;
						
					case "BC":
						probability *= diffData.getpBC();
						break;
						
					case "CA":
						probability *= diffData.getpCA();
						break;
						
					case "CB":
						probability *= diffData.getpCB();
						break;
						
					case "CC":
						probability *= diffData.getpCC();
						break;
						
					}
					
					if (k == seq.length() - 1) {
						switch(chs[k]) {
						case 'A':
							phase = 	(lseq.getnA() - 1) * diffData.getASpacing() +
										lseq.getnB() * diffData.getBSpacing() +
										lseq.getnC() * diffData.getCSpacing();
							break;
							
						case 'B':
							phase = 	lseq.getnA() * diffData.getASpacing() +
										(lseq.getnB() - 1) * diffData.getBSpacing() +
										lseq.getnC() * diffData.getCSpacing();
							break;
							
						case 'C':
							phase = 	lseq.getnA() * diffData.getASpacing() +
										lseq.getnB() * diffData.getBSpacing() +
										(lseq.getnC() - 1) * diffData.getCSpacing();
							break;
							
									
						}
					}
				}
			}
			
			lseq.setProbabilityProduct(probability);
			lseq.setPhaseTermR(phase);
		}
		return;
	}
	
	
	////////////////////////////////////////////////////////////////////
	//				INNER CLASSES
	////////////////////////////////////////////////////////////////////
	
	class buttonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!paValue.getText().isEmpty()) {
				diffData.setpA(Double.parseDouble(paValue.getText()));
			}
			if (!pbValue.getText().isEmpty()) {
				diffData.setpB(Double.parseDouble(pbValue.getText()));
			}
			if (!pcValue.getText().isEmpty()) {
				diffData.setpC(Double.parseDouble(pcValue.getText()));
			}
			if (!paaValue.getText().isEmpty()) {
				diffData.setpAA(Double.parseDouble(paaValue.getText()));
			}
			if (!pbbValue.getText().isEmpty()) {
				diffData.setpBB(Double.parseDouble(pbbValue.getText()));
			}
			if (!pccValue.getText().isEmpty()) {
				diffData.setpCC(Double.parseDouble(pccValue.getText()));
			}
			if (!pabValue.getText().isEmpty()) {
				diffData.setpAB(Double.parseDouble(pabValue.getText()));
			}
			if (!pbaValue.getText().isEmpty()) {
				diffData.setpBA(Double.parseDouble(pbaValue.getText()));
			}
			if (!pacValue.getText().isEmpty()) {
				diffData.setpAC(Double.parseDouble(pacValue.getText()));
			}
			if (!pcaValue.getText().isEmpty()) {
				diffData.setpCA(Double.parseDouble(pcaValue.getText()));
			}
			if (!pbcValue.getText().isEmpty()) {
				diffData.setpBC(Double.parseDouble(pbcValue.getText()));
			} else {
				pBC = 0;
			}
			if (!pcbValue.getText().isEmpty()) {
				diffData.setpCB(Double.parseDouble(pcbValue.getText()));
			}
			
			
			Combinations cmb = new Combinations(diffData.getNumLayers(), diffData);
			cmb.createCombinations(diffData.getNumLayers());
			ArrayList<Combinations.LayerSequence> ABCCombinations = cmb.getABCCombinations();
			calculateProbabilities(ABCCombinations);

			IntensityCalculation iCalc = new IntensityCalculation(diffData, ABCCombinations);
			iCalc.calculateIntensity();
			thisPane.dispose();
			
		}
	}
	
}
