package diffpattern;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import layerSF.LayerComponent;
import layerSF.LayerComponent.LayerAtomData;
import main.Strat3Main;

public class DiffractionPanel extends JInternalFrame {

	DiffractionPanel mainPane;
	
	// GUI components
	JLabel numLabel = new JLabel("No. of Layers:");
	JTextField numValue = new JTextField("", 5);
	JLabel saLabel = new JLabel("Spacing for A");
	JTextField saValue = new JTextField("", 5);
	JLabel sbLabel = new JLabel("Spacing for B");
	JTextField sbValue = new JTextField("", 5);
	JLabel scLabel = new JLabel("Spacing for C");
	JTextField scValue = new JTextField("", 5);
	
	JLabel waveLabel = new JLabel("Wavelength of radiation");
	JTextField waveValue = new JTextField("", 5);
	JLabel startLabel = new JLabel("1st r* value");
	JTextField startValue = new JTextField("0.05", 5);
	JLabel endLabel = new JLabel("last r* value");
	JTextField  endValue= new JTextField("0.5", 5);
	JLabel deltaLabel = new JLabel("r* interval");
	JTextField deltaValue = new JTextField("0.01", 5);

	JLabel brekLabel = new JLabel("Fraction of r*=0 to remove");
	JTextField brekValue = new JTextField("0.0", 5);
	JLabel normLabel = new JLabel("output normalisation");
	JTextField normValue = new JTextField("100", 5);
	
	JRadioButton randomLP = new JRadioButton("Random", true);
	JRadioButton exPlaneLP = new JRadioButton("Extended");
	JCheckBox restricted = new JCheckBox("Restricted");
	
	JButton probabilities = new JButton("Define Probabilities");

	static final int xOffset = 200, yOffset = 50;
	
	private int numLayers = 0;
	private int numComponents = Strat3Main.getMainFrame().getNumComponents();
	private double aSpacing = 0.0, bSpacing = 0.0, cSpacing = 0.0;
	private double llambda = 0.0;
	private double startRStar = 0.0, maxRStar = 0.0;
	private double deltaRStar = 0.05;
	private double backStop = 0.0;
	private double normMax = 100.0;
	
	/*
	 * When noDuplicateCombinations is true...
	 * If a combination sequence has the same end layers
	 * and the middle layers (excluding ends) are the same,
	 * although may be in a different order, they will have the same 
	 * phase term and will be excluded from the sums although the 
	 * contribution to the final intensity will be made as the 
	 * number of duplicates will be counted and used to multiply 
	 * the corresponding intensity term.
	 * This greatly reduces the number of combinations. 
	 * 
	 * BUT NOTE:
	 * Although the phase term is the same, the probability terms may 
	 * not be the same so the calculated intensity may be different.
	 */
	
	public DiffractionPanel(String title) {
		super(title, 
	              true, //resizable
	              true, //closable
	              true, //maximizable
	              true);//iconifiable
		
		mainPane = this;
		
		initialiseLayout(getLayerThickness());
		
	    setSize(new Dimension(300, 450));
	    setLocation(xOffset, yOffset);
		setVisible(true);
	}

	/*
	 * Returned as Double array, LayerA = [0], LayerB = [1], LayerC = [2]
	 */
	private ArrayList<Double> getLayerThickness() {
		ArrayList<Double> sABC = null;
		ArrayList<LayerComponent> layers = Strat3Main.getMainFrame().getLayers();
		double maxZ;
		
		if (!layers.isEmpty()) {
			sABC = new ArrayList<Double>();
		}
		
		for (Iterator it = layers.iterator(); it.hasNext();) {
			LayerComponent layC = (LayerComponent) it.next();
			maxZ = -1;
			ArrayList<LayerAtomData> lads = layC.getComponentAtomData();
			for (Iterator it2 = lads.iterator(); it2.hasNext();) {
				LayerAtomData lad = (LayerAtomData) it2.next();
				double aZ = Double.parseDouble(lad.getzCoord());
				if (aZ > maxZ) {
					maxZ = aZ;
				}
			}
			
			sABC.add(maxZ);
		}
		
		return sABC;
	}
	
	private void initialiseLayout(ArrayList<Double> sABC) {
		JPanel mainPane = new JPanel();
		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
		mainPane.setPreferredSize(new Dimension(300, 450));

		JPanel layerDataPanel = new JPanel();
		layerDataPanel.setBorder(BorderFactory.createTitledBorder("Layer Data"));
		layerDataPanel.setLayout(new BoxLayout(layerDataPanel, BoxLayout.Y_AXIS));
		layerDataPanel.setPreferredSize(new Dimension(300, 400));
		
		JPanel numLayersPanel = new JPanel();
		numLayersPanel.setLayout(new FlowLayout());
		numLayersPanel.setPreferredSize(new Dimension(200, 100));
		numLayersPanel.add(numLabel);
		numLayersPanel.add(numValue);
		JPanel saSpacing = new JPanel();
		saSpacing.setLayout(new FlowLayout());
		saSpacing.setPreferredSize(new Dimension(200, 100));
		saSpacing.add(saLabel);
		saSpacing.add(saValue);
		if (sABC != null) {
			saValue.setText(""+sABC.get(0));
		}
		
		JPanel sbSpacing = new JPanel();
		sbSpacing.setLayout(new FlowLayout());
		sbSpacing.setPreferredSize(new Dimension(200, 100));
		sbSpacing.add(sbLabel);
		sbSpacing.add(sbValue);
		if (sABC != null && numComponents > 1) {
			sbValue.setText(""+sABC.get(1));
		} else {
			sbValue.setText("0.0");
		}
		
		JPanel scSpacing = new JPanel();
		scSpacing.setLayout(new FlowLayout());
		scSpacing.setPreferredSize(new Dimension(200, 100));
		scSpacing.add(scLabel);
		scSpacing.add(scValue);
		if (sABC != null && numComponents > 2) {
			scValue.setText(""+sABC.get(2));
		} else {
			scValue.setText("0.0");
		}
		
		layerDataPanel.add(numLayersPanel);
		layerDataPanel.add(saSpacing);
		layerDataPanel.add(sbSpacing);
		layerDataPanel.add(scSpacing);

		JPanel diffDataPanel = new JPanel();
		diffDataPanel.setBorder(BorderFactory.createTitledBorder("r* Data"));
		diffDataPanel.setLayout(new BoxLayout(diffDataPanel, BoxLayout.Y_AXIS));
		diffDataPanel.setPreferredSize(new Dimension(300, 400));

		JPanel lpData = new JPanel();
		lpData.setBorder(BorderFactory.createTitledBorder("LP Factor"));
		lpData.setLayout(new BoxLayout(lpData, BoxLayout.X_AXIS));
		lpData.setPreferredSize(new Dimension(150, 400));
		lpData.add(randomLP);
		lpData.add(exPlaneLP);
		lpData.add(restricted);
		ButtonGroup bg = new ButtonGroup();
		bg.add(randomLP);
		bg.add(exPlaneLP);
		
		JPanel rStarPanel = new JPanel();
		rStarPanel.setLayout(new BoxLayout(rStarPanel, BoxLayout.X_AXIS));
		rStarPanel.setPreferredSize(new Dimension(200, 100));
		rStarPanel.add(waveLabel);
		rStarPanel.add(waveValue);
		JPanel startPanel = new JPanel();
		startPanel.setLayout(new FlowLayout());
		startPanel.setPreferredSize(new Dimension(200, 100));
		startPanel.add(startLabel);
		startPanel.add(startValue);
		JPanel endPanel = new JPanel();
		endPanel.setLayout(new FlowLayout());
		endPanel.setPreferredSize(new Dimension(200, 100));
		endPanel.add(endLabel);
		endPanel.add(endValue);
		JPanel deltaPanel = new JPanel();
		deltaPanel.setLayout(new FlowLayout());
		deltaPanel.setPreferredSize(new Dimension(200, 100));
		deltaPanel.add(deltaLabel);
		deltaPanel.add(deltaValue);
		
		diffDataPanel.add(rStarPanel);
		diffDataPanel.add(startPanel);
		diffDataPanel.add(endPanel);
		diffDataPanel.add(deltaPanel);
		
		JPanel outputPanel = new JPanel();
		outputPanel.setBorder(BorderFactory.createTitledBorder("Output Data"));
		outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.Y_AXIS));
		outputPanel.setPreferredSize(new Dimension(300, 400));

		JPanel fracPanel = new JPanel();
		fracPanel.setLayout(new FlowLayout());
		fracPanel.setPreferredSize(new Dimension(200, 100));
		fracPanel.add(brekLabel);
		fracPanel.add(brekValue);
		JPanel normPanel = new JPanel();
		normPanel.setLayout(new FlowLayout());
		normPanel.setPreferredSize(new Dimension(200, 100));
		normPanel.add(normLabel);
		normPanel.add(normValue);

		outputPanel.add(fracPanel);
		outputPanel.add(normPanel);
		
		JPanel buttonPanel = new JPanel();
		probabilities.addActionListener(new buttonListener());
		buttonPanel.add(probabilities);

		mainPane.add(layerDataPanel);
		mainPane.add(diffDataPanel);
		mainPane.add(lpData);
		mainPane.add(outputPanel);
		mainPane.add(buttonPanel);
		
		this.setContentPane(mainPane);
	}

	////////////////////////////////////////////////////////////////////
	
	
	////////////////////////////////////////////////////////////////////
	//				INNER CLASSES
	////////////////////////////////////////////////////////////////////
	
	class buttonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			DiffractionData dd = new DiffractionData();
			
			dd.setRandomLP(randomLP.isSelected());
			dd.setNoDuplicateCombinations(restricted.isSelected());
			
			if (!numValue.getText().isEmpty()) {
				int numLayers = Integer.parseInt(numValue.getText());
				if (numLayers > 10 && !dd.isNoDuplicateCombinations()) {
					JOptionPane.showMessageDialog(Strat3Main.getMainFrame(), "Max no. of layers = 10", "Reseting No. layers", JOptionPane.INFORMATION_MESSAGE);
					numLayers = 10;
				}
				dd.setNumLayers(numLayers);
			}
			if (!saValue.getText().isEmpty()) {
				dd.setASpacing(Double.parseDouble(saValue.getText()));
			}
			if (!sbValue.getText().isEmpty()) {
				dd.setBSpacing(Double.parseDouble(sbValue.getText()));
			}
			if (!scValue.getText().isEmpty()) {
				dd.setCSpacing(Double.parseDouble(scValue.getText()));
			}
			if (!waveValue.getText().isEmpty()) {
				dd.setLlambda(Double.parseDouble(waveValue.getText()));
			}
			if (startValue.getText().isEmpty() || startValue.getText().equals("0")) {
				dd.setStartRStar(0.02);
			} else {
				dd.setStartRStar(Double.parseDouble(startValue.getText()));
			}
			
			maxRStar = Strat3Main.getMainFrame().getMaxRStar();
			if (!endValue.getText().isEmpty()) {
				double endRStar = Double.parseDouble(endValue.getText());
				if (endRStar < maxRStar) {
					maxRStar = endRStar;
				}
				
				dd.setEndRStar(endRStar);
			}
			
			if (!deltaValue.getText().isEmpty()) {
				dd.setDeltaRStar(Double.parseDouble(deltaValue.getText()));
			}
			if (!brekValue.getText().isEmpty()) {
				dd.setBackStopFactor(Double.parseDouble(brekValue.getText()));
			}
			if (!normValue.getText().isEmpty()) {
				dd.setNormFactor(Double.parseDouble(normValue.getText()));
			}
			
			Probabilities prob = new Probabilities("Enter Layer proportions & probabilities", dd);
			Strat3Main.getMainFrame().getDesktop().add(prob);
			prob.moveToFront();
			
			mainPane.dispose();
		}
	}

}
