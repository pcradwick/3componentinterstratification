package diffpattern;

import main.Strat3Main;

public class DiffractionData {

	private int numLayers = 0;
	private double aSpacing = 0.0, bSpacing = 0.0, cSpacing = 0.0;
	private double llambda = 0.0;
	private double startRStar = 0.0, endRStar = 0.0;
	private double deltaRStar = 0.05;
	private double backStopFactor = 0.0;
	private double normFactor = 100.0;

	private double pA = 0.0, pB = 0.0, pC = 0.0;
	private double pAA = 0.0, pBB = 0.0, pCC = 0.0;
	private double pAB = 0.0, pBA = 0.0, pAC = 0.0, pCA = 0.0, pBC = 0.0, pCB = 0.0;
	
	private boolean randomLP = true;
	private boolean noDuplicateCombinations = false;

	public DiffractionData() {	}

	public int getNumLayers() {
		return numLayers;
	}

	public void setNumLayers(int numLayers) {
		this.numLayers = numLayers;
	}

	public double getASpacing() {
		return aSpacing;
	}

	public void setASpacing(double aSpacing) {
		this.aSpacing = aSpacing;
	}

	public double getBSpacing() {
		return bSpacing;
	}

	public void setBSpacing(double bSpacing) {
		this.bSpacing = bSpacing;
	}

	public double getCSpacing() {
		return cSpacing;
	}

	public void setCSpacing(double cSpacing) {
		this.cSpacing = cSpacing;
	}

	public double getLlambda() {
		return llambda;
	}

	public void setLlambda(double llambda) {
		this.llambda = llambda;
	}

	public double getStartRStar() {
		return startRStar;
	}

	public void setStartRStar(double startRStar) {
		this.startRStar = startRStar;
	}

	public double getEndRStar() {
		return endRStar;
	}

	public void setEndRStar(double endRStar) {
		this.endRStar = endRStar;
	}

	public double getDeltaRStar() {
		return deltaRStar;
	}

	public void setDeltaRStar(double deltaRStar) {
		this.deltaRStar = deltaRStar;
	}

	public double getBackStopFactor() {
		return backStopFactor;
	}

	public void setBackStopFactor(double bs) {
		this.backStopFactor = bs;
	}

	public double getNormFactor() {
		return normFactor;
	}

	public void setNormFactor(double normMax) {
		this.normFactor = normMax;
	}

	public double getpA() {
		return pA;
	}

	public void setpA(double pA) {
		this.pA = pA;
	}

	public double getpB() {
		return pB;
	}

	public void setpB(double pB) {
		this.pB = pB;
	}

	public double getpC() {
		return pC;
	}

	public void setpC(double pC) {
		this.pC = pC;
	}

	public double getpAA() {
		return pAA;
	}

	public void setpAA(double pAA) {
		this.pAA = pAA;
	}

	public double getpBB() {
		return pBB;
	}

	public void setpBB(double pBB) {
		this.pBB = pBB;
	}

	public double getpCC() {
		return pCC;
	}

	public void setpCC(double pCC) {
		this.pCC = pCC;
	}

	public double getpAB() {
		return pAB;
	}

	public void setpAB(double pAB) {
		this.pAB = pAB;
	}

	public double getpBA() {
		return pBA;
	}

	public void setpBA(double pBA) {
		this.pBA = pBA;
	}

	public double getpAC() {
		return pAC;
	}

	public void setpAC(double pAC) {
		this.pAC = pAC;
	}

	public double getpCA() {
		return pCA;
	}

	public void setpCA(double pCA) {
		this.pCA = pCA;
	}

	public double getpBC() {
		return pBC;
	}

	public void setpBC(double pBC) {
		this.pBC = pBC;
	}

	public double getpCB() {
		return pCB;
	}

	public void setpCB(double pCB) {
		this.pCB = pCB;
	}

	public boolean isRandomLP() {
		return randomLP;
	}

	public void setRandomLP(boolean randomLP) {
		this.randomLP = randomLP;
	}

	public boolean isNoDuplicateCombinations() {
		return noDuplicateCombinations;
	}

	public void setNoDuplicateCombinations(boolean noDuplicateCombinations) {
		this.noDuplicateCombinations = noDuplicateCombinations;
	}
	
}
