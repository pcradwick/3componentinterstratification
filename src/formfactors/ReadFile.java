package formfactors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import org.apache.commons.lang3.exception.ExceptionUtils;

import utilities.Logging;
import utilities.SystemSettings;

	
public class ReadFile {
	private SystemSettings ss = SystemSettings.instance();

	public ReadFile() {}

	public ArrayList<OneConstantsRow> readFileData() {
		ArrayList<OneConstantsRow> data = new ArrayList<OneConstantsRow>();
		String fileName = ss.getWorkingDir()+ss.getFileSeparator()+
				"resources"+ss.getFileSeparator() +"FormFactorConstants.txt";
		File file = new File(fileName);
		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new FileReader(file));
		
			String oneLine;
			int lineNo = 1;
			while ((oneLine = br.readLine()) != null) {
				if (lineNo > 1) {
					OneConstantsRow row = splitLine(oneLine);
					data.add(row);
				}
				++lineNo;
			}
		} catch (Exception e) {
			String stackTrace = ExceptionUtils.getStackTrace(e);
			Logging.writeToLog(true, e.toString()+" in ReadFile.readFileData\nStackTrace:\n"+stackTrace, Logging.ERROR);
		} finally {
			if (br != null ) {
				try {
					br.close();
				} catch(Exception e) {
					String stackTrace = ExceptionUtils.getStackTrace(e);
					Logging.writeToLog(true, e.toString()+" in ReadFile.readFileData closing BufferedReader\nStackTrace:\n"+stackTrace, Logging.ERROR);
				}
			}
		}
		return data;
	}
	
	public OneConstantsRow splitLine(String line) {
		OneConstantsRow ocr = new OneConstantsRow();
		String modLine = line.replaceAll("[^\\x00-\\xFF]", " ");	// replaces non ASCII chars
		modLine = modLine.replaceAll("\\s", " ");					// replaces ASCII whitespace e.g. \t
		String[] splitRow = modLine.split(" ");
		String[] trimmedRow = new String[10];
		
		int index = -1;
		for (int i=0; i<splitRow.length; ++i) {
			if (splitRow[i].isEmpty()) {
				continue;
			} else {
				trimmedRow[++index] = splitRow[i].trim();
			}
		}
					
		ocr.setLabel(trimmedRow[0]);
		ocr.setA1(trimmedRow[1]);
		ocr.setB1(trimmedRow[2]);
		ocr.setA2(trimmedRow[3]);
		ocr.setB2(trimmedRow[4]);
		ocr.setA3(trimmedRow[5]);
		ocr.setB3(trimmedRow[6]);
		ocr.setA4(trimmedRow[7]);
		ocr.setB4(trimmedRow[8]);
		ocr.setCc(trimmedRow[9]);
		return ocr;
	}

}
