package formfactors;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.commons.lang3.exception.ExceptionUtils;

import layerSF.LayerComponent;
import layerSF.LayerComponent.LayerAtomData;
import main.Strat3Main;
import utilities.Logging;
import utilities.SystemSettings;

public class FormFactorPanel extends JInternalFrame {

	ArrayList<String> labelData;
	LayerComponent thisLayer = null;
	
	// icons
	String slash = SystemSettings.instance().getFileSeparator();
	String imageDir = SystemSettings.instance().getWorkingDir()+
								slash+"resources"+slash+"images"+slash+"icons";
	Icon tick  = new ImageIcon(imageDir+slash+"greentick2.png");
	Icon cross = new ImageIcon(imageDir+slash+"redcross.png");
	Icon query = new ImageIcon(imageDir+slash+"questionmark.png");
	
	// GUI components
	JLabel nameLabel = new JLabel("Layer name:");
	JTextField layerName = new JTextField("", 20);
	JComboBox<String> atomsList = new JComboBox<String>();
	JTextArea selectedAtoms = new JTextArea(5, 10);
	JTextArea helpTextArea = new JTextArea(5, 43);
	JButton checkForFile = new JButton("FilePresent",query);
	JLabel rClickText = new JLabel();

	
	String helpText = "Use Combox to select atom components for this Layer. Then "
			+ "Right Click in the blank area above to Display the scattering factor curves."
			+ "Use popup menu or the menu bar iteString titlem to calculate Layer Structure factor data."
			+ "Finally use main menu to calculate the diffraction pattern";
    static int openFrameCount = 0;
    static final int xOffset = 50, yOffset = 50;
    
    JPanel mainPane = null;
    FormFactorPopupMenu popupPane;
    
	public FormFactorPanel(String title, LayerComponent lc) {
		super(title, 
	              true, //resizable
	              true, //closable
	              true, //maximizable
	              true);//iconifiable
	 
		++openFrameCount;
		thisLayer = lc;
		
	    initialiseLayout();
	    enableFields(true);
	    
	    setSize(new Dimension(500, 300));
	    
	    int x = xOffset*openFrameCount;
	    int y = yOffset*openFrameCount;
	    
	    setLocation(x, y);
	    
	}

	private void initialiseLayout() {
		checkForFile.addActionListener(new NameListener());
		atomsList.addItemListener(new ComboBoxListener());
		loadComboData();
		
		popupPane = new FormFactorPopupMenu(thisLayer);
		mainPane = popupPane;	// Displays popup menu on Right Click
		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
		
		// Help Text Area
		JPanel helpTextPanel = new JPanel();
		helpTextPanel.setBorder(BorderFactory.createTitledBorder("Instructions"));
		helpTextArea.setText(helpText);
		helpTextArea.setLineWrap(true);
		helpTextArea.setWrapStyleWord(true);
		helpTextPanel.add(helpTextArea);
		
		// Selected Atoms TextArea Panel
		JPanel SelectedAtomsPanel = new JPanel();
		SelectedAtomsPanel.setBorder(BorderFactory.createTitledBorder("Selected Atoms"));
		SelectedAtomsPanel.setLayout(new BoxLayout(SelectedAtomsPanel, BoxLayout.X_AXIS));
		SelectedAtomsPanel.setPreferredSize(new Dimension(200, 100));
		SelectedAtomsPanel.add(selectedAtoms);

		// RightClick area
		JPanel popupMenuArea = new JPanel();
		popupMenuArea.setLayout(new BoxLayout(popupMenuArea, BoxLayout.X_AXIS));
		popupMenuArea.setPreferredSize(new Dimension(200, 100));
		rClickText.setText("Right click area");
		popupMenuArea.add(rClickText);
		
		// AtomList ComboBox Panel
		JPanel atomComboPanel = new JPanel();
		atomComboPanel.setBorder(BorderFactory.createTitledBorder("Select Atoms"));
		atomComboPanel.setLayout(new BoxLayout(atomComboPanel, BoxLayout.X_AXIS));
		atomComboPanel.setPreferredSize(new Dimension(200, 50));
		atomComboPanel.add(atomsList);
		
		// Layer Name Panel
		JPanel layerNameArea = new JPanel();
		layerNameArea.setLayout(new FlowLayout());
		layerNameArea.setPreferredSize(new Dimension(200, 100));
		layerNameArea.add(nameLabel);
		layerNameArea.add(layerName);
		checkForFile.setHorizontalTextPosition(SwingConstants.LEFT);
		layerNameArea.add(checkForFile);
		
		
		mainPane.add(layerNameArea);
		mainPane.add(SelectedAtomsPanel);
		mainPane.add(atomComboPanel);
		mainPane.add(popupMenuArea);
		mainPane.add(helpTextPanel);
		mainPane.setVisible(true);
		
		this.setContentPane(mainPane);
		
		layerName.requestFocusInWindow();
	}
	
	private void enableFields(boolean state) {
		atomsList.setEnabled(state);
		selectedAtoms.setEnabled(state);
	}
	
	private void loadComboData() {
		labelData = Strat3Main.getMainFrame().getLabels();
		atomsList.addItem("None");
		
		for (String str : labelData) {
			atomsList.addItem(str);
		}
	}
	
	private void refresh(String atom) {
		String str = selectedAtoms.getText();
		if (str.isEmpty()) {
			str += atom;
		} else {
			str += ", "+atom;
		}
		
		selectedAtoms.setText(str);
	}
	
	// Populate thisLayer from file data
	private void populateLayerFromFile(String name) {
		try {
			List<String> lines = Files.readAllLines(Paths.get(thisLayer.getFileName()));

			for(String line : lines) {
				String [] words = line.split(" ");
				switch (words[0]) {
				case "LayerNumber":
					continue;

				case "LayerName":
					thisLayer.setName(words[2]);
					break;

				case "LayerNumAtoms":
					thisLayer.setNumAtoms(words[2]);
					break;

				case "MaxRStar":
					thisLayer.setMaxRStar(words[2]);
					break;

				case "AtomTypes":
					for (int i=2; i< words.length; ++i) {
						thisLayer.getComponentAtomTypes().add(words[i]);
					}
					break;

				case "Atom":
					LayerAtomData atom = thisLayer.new LayerAtomData();
					atom.setAtomNumber(words[1]);
					atom.setName(words[2]);
					atom.setNumPerLayer(words[3]);
					atom.setzCoord(words[4]);
					atom.setbFactor(words[5]);
					thisLayer.getComponentAtomData().add(atom);
					break;

				default:
					Logging.writeToLog(true, "Unknown line: "+line+" in file: "+name, Logging.ERROR);
				}

			}
		} catch(Exception e) {
			String stackTrace = ExceptionUtils.getStackTrace(e);
			Logging.writeToLog(true, e.toString()+" reading file in LayerDialog.populateLayout. StackTrace:\n"+stackTrace, Logging.ERROR);
		}

		thisLayer.setFilePopulated(true);
		
		return;
	}
	
	private boolean checkForFile(String name) {
		String slash = SystemSettings.instance().getFileSeparator();

		String fileName = thisLayer.getLayoutDir()+slash+name;
		thisLayer.setFileName(fileName);
		
		File f = new File(fileName);
		if (f.exists()) {
			thisLayer.setLayerFileExists(true);
		} else {
			Logging.writeToLog(false, "No layer file for "+name, Logging.INFO);
		}
		
		return thisLayer.isLayerFileExists();
	}
	///////////////////////////////////////////////////////////
	//				INNER CLASSES
	///////////////////////////////////////////////////////////
	
	  private final class ComboBoxListener implements ItemListener {
		    // This is called when an item is changed or clicked - if more... re-populate ComboBox
		    @Override
		    public void itemStateChanged(ItemEvent itemEvent) {
		      if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
		        String itemSelected = (String) itemEvent.getItem();
		        
		        if (!itemSelected.equals("None")) {
		        	thisLayer.getComponentAtomTypes().add(itemSelected);
			        refresh(itemSelected);
			        Logging.writeToLog(true, "Selected Label = "+itemSelected, Logging.DEBUG);
		        }
		        
		      }
		    }

		  }
 
		class NameListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				thisLayer.setName(layerName.getText());
				if (layerName.getText().isEmpty()) {
					JOptionPane.showMessageDialog(Strat3Main.getMainFrame(), "Enter a Layer name", "Missing Layer Name", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				if (checkForFile(layerName.getText())) {
					enableFields(false);
					if (!thisLayer.isFilePopulated()) {
						populateLayerFromFile(layerName.getText());
					}
					
					popupPane.setMenuItem("Check/Modify Layer Atom Data");
					checkForFile.setIcon(tick);
					rClickText.setText("File exists. Right click here to check/modify layer data");
				} else {
					checkForFile.setIcon(cross);
					rClickText.setText("File doesn't exist. Right click here to define Layer structure");
					popupPane.setMenuItem("Collect Layer Atom Data");
				}
			}
		}

}
