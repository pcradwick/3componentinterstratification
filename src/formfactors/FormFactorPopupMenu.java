package formfactors;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

import layerSF.CalculateLayerSF;
import layerSF.LayerComponent;
import layerSF.LayerDialog;
import main.Strat3Main;
import utilities.Logging;
import utilities.SystemSettings;

/*
 *  1.	Display popup menu with menuItems: "Delete"
 *  2.	Which then displays "Are you sure..."
 *  3.	Send message to Server to move the file
 *  		from file store to "DeletedFiles" directory.
 */

public class FormFactorPopupMenu extends JPanel {

	//	public PopupCallbackData Callback;				// The interface
	LayerComponent thisLayer;
	
	public JPopupMenu popup;

	private String atomMenuItem = null;
	
	public FormFactorPopupMenu(LayerComponent lc) {
		popup = new JPopupMenu();
		thisLayer = lc;
		addMouseListener(new PopupMouseListener());
	}

	private void setMenu() {
		popup.removeAll();
		PopupActionListener menuListener = new PopupActionListener();
		SystemSettings ss =SystemSettings.instance();
		String imagePath = ss.getWorkingDir()+ss.getFileSeparator()+"resources"+
				ss.getFileSeparator()+"images"+ss.getFileSeparator()+"icons"+ss.getFileSeparator();

		JMenuItem item;
		popup.add(item = new JMenuItem("Display Form Factor Curves", new ImageIcon(imagePath+"search_small.gif")));
		item.setHorizontalTextPosition(JMenuItem.RIGHT);
		item.setFont(new Font("Times", Font.BOLD, 15));
		item.addActionListener(menuListener);
		popup.addSeparator();
		popup.add(item = new JMenuItem(getMenuItem(), new ImageIcon(imagePath+"back.gif")));
		item.setHorizontalTextPosition(JMenuItem.RIGHT);
		item.setFont(new Font("Times", Font.BOLD, 15));
		item.addActionListener(menuListener);
		popup.addSeparator();
		popup.add(item = new JMenuItem("Accept File Layer Atom Data", new ImageIcon(imagePath+"back.gif")));
		item.setHorizontalTextPosition(JMenuItem.RIGHT);
		item.setFont(new Font("Times", Font.BOLD, 15));
		item.addActionListener(menuListener);

		popup.setBorder(new BevelBorder(BevelBorder.RAISED));
		popup.setBorderPainted(true);

	}
	
	private String getMenuItem() {
		return atomMenuItem;
	}
	
	public void setMenuItem(String item) {
		atomMenuItem = item;
		setMenu();
	}

	///////////////////////////////////////////////////////////////
	//				INNER CLASSES
	///////////////////////////////////////////////////////////////

	// Actions popup menu items selection
	public class PopupActionListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
//			Logging.writeToLog(true, "Popup menu item ["
//					+ event.getActionCommand() + "] was pressed.", Logging.DEBUG);

			Logging.writeToLog(false, "Calculate and Store scattering factor curves", Logging.DEBUG);
			
			Set<String> atoms = Strat3Main.getMainFrame().getUniqueAtoms();
			FormFactorCurves ffc = new FormFactorCurves(atoms);
			
			String[] action = event.getActionCommand().split(" ");
			switch (action[0]) {
			case "Display":				// Display Form Factor Curves
				Logging.writeToLog(false, "Plotting scattering factor curves", Logging.DEBUG);
				ffc.plotCurve(atoms);
				break;
				
			case "Check/Modify":
			case "Collect":				// Create Layer Structure Factor Data
				new LayerDialog(Strat3Main.getMainFrame(), thisLayer);
				break;
				
			case "Accept":
				break;
				
			}
				

		}

	}
	
	// Displays popup menu
	public class PopupMouseListener extends MouseAdapter
	{
		public void mouseClicked(MouseEvent mev) {
//			Logging.writeToLog(true, "MenuItemMouseListener : mouse clicked", Logging.DEBUG);
			checkPopup(mev);
		}

		@Override
		public void mousePressed(MouseEvent mev) {
//			Logging.writeToLog(true, "MenuItemMouseListener : mouse pressed", Logging.DEBUG);
			checkPopup(mev);
		}

		@Override
		public void mouseReleased(MouseEvent mev) {
//			Logging.writeToLog(true, "MenuItemMouseListener : mouse released", Logging.DEBUG);
			checkPopup(mev);
		}

		private void checkPopup(MouseEvent e) {
			if (e.isPopupTrigger()) {
				popup.show(FormFactorPopupMenu.this, e.getX(), e.getY());
			}
		}
	};

}
