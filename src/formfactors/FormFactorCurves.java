package formfactors;

import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import main.Strat3Main;
import utilities.Logging;


public class FormFactorCurves {
	/*
	 * Each row of formFactorConstants contains: (OneConstantsRow)
	 * 		Atom label, a1, b1, a2, b2, a3, b3, a4, b4, c
	 * 
	 * The curves are plotted using data in the paper:
	 * http://lampx.tugraz.at/~hadley/ss1/crystaldiffraction/atomicformfactors/formfactors.php
	 * 
	 * 		f(q) = Sum[i]{a[i].exp(-b[i](q/4pi)**2} + c		{sum over i}
	 * 
	 * They are plotted as a function of q where:		units = 1/Angstrom
	 * 
	 * 		q = 4.pi.sin(theta)/llambda = 2.pi.r*		(q = 0 -> 25)
	 * 
	 * 		r* = 2.sin(theta)/llambda = q/(2.pi)		(r* = 0 -> 3.9789)
	 * 
	 * 		sin(theta)/llambda = q/(4.pi) = (r*)/2		(0 -> 1.9894)
	 * 
	 * Layer Structure Factor and Diffraction curves are plotted as a function of r*
	 */

	private ArrayList<RStarAndF> oneCurve = null;
	private int maxQ = 25;				// equivalent to r* = 3.98
	
	public FormFactorCurves(Set<String> atoms) {
		
		double twoPi = 2* Math.PI;
		
		// Generate atom scattering factor curves for the selected atoms
		for (Iterator<String> it = atoms.iterator(); it.hasNext();) {
			String atom = (String) it.next();
			oneCurve = new ArrayList<RStarAndF>();
			
			for (int q = 0; q < maxQ; ++q) {
				double value = calculatePoint(atom, q);
				RStarAndF rf = new RStarAndF();
				rf.setrStar((double) q/twoPi);
				rf.setF(value);
				oneCurve.add(rf);
			}
			
			Strat3Main.getMainFrame().addfCurve(atom, oneCurve);
		}
	}

	private double calculatePoint(String atom, double q) {
		OneConstantsRow constants = findRow(atom);
		if (constants == null) {
			String msg = "Unable to find constants row for atom:"+atom;
			Logging.writeToLog(true, msg, Logging.ERROR);
			return Double.NaN;
		}
		
		double[] ai = {Double.parseDouble(constants.getA1()), 
						Double.parseDouble(constants.getA2()),
						Double.parseDouble(constants.getA3()),
						Double.parseDouble(constants.getA4())};
		
		double[] bi = {Double.parseDouble(constants.getB1()), 
						Double.parseDouble(constants.getB2()),
						Double.parseDouble(constants.getB3()),
						Double.parseDouble(constants.getB4())};
		
		double sum = 0;
		for (int i = 0; i < 4; i++) {
			double a = ai[i];
			double b = bi[i];
			sum += a * Math.exp(-b * (Math.pow(q/(4*Math.PI), 2)));
		}
		
		sum += Double.parseDouble(constants.getCc());
		
		// Round sum to 4 decimal places
		DecimalFormat df4 = new DecimalFormat("####.####");
		double round4 = Double.valueOf(df4.format(sum));
		return round4;
	}
	
	private OneConstantsRow findRow(String atom) {
		ArrayList<OneConstantsRow> formFactorConstants = Strat3Main.getMainFrame().getFormFactorConst();
		OneConstantsRow row = null;
		
		for (Iterator<OneConstantsRow> it = formFactorConstants.iterator(); it.hasNext();) {
			OneConstantsRow ocr = (OneConstantsRow) it.next();
			if (ocr.getLabel().equals(atom)) {
				row = ocr;
				break;
			}
		}
		
		return row;
	}
	
	public void plotCurve(Set<String> atoms) {
		Map<String, ArrayList<RStarAndF>> data = Strat3Main.getMainFrame().getfCurves();
		
		for (Iterator<String> it = atoms.iterator(); it.hasNext();) {
			String atom = (String) it.next();
			ArrayList<RStarAndF> fCurve = data.get(atom);
			
			CurvePlotter cp = new CurvePlotter("Scattering Factor Curve for ", atom, fCurve);
			cp.pack();
			cp.setVisible(true);
		}
	}
	
	//////////////////////////////////////////////////////////////////////
	//					INNER CLASSES
	//////////////////////////////////////////////////////////////////////

	public class CurvePlotter extends ApplicationFrame {
		
		public CurvePlotter(String title, String atom, ArrayList<RStarAndF> curve) {
			super(title);
			
			XYSeries series = new XYSeries("r*");
			double q = -1;
			for (Iterator<RStarAndF> it = curve.iterator(); it.hasNext();) {
				RStarAndF pt = (RStarAndF) it.next();
				series.add(pt.getrStar(), pt.getF());
			}
			
			XYSeriesCollection data = new XYSeriesCollection(series);
			JFreeChart chart = ChartFactory.createXYLineChart(
					title+atom,
					"q =2(pi)r*",
					"f(q)",
					data,
					PlotOrientation.VERTICAL,
					false,
					false,
					false
					);
			
			ChartPanel chartPanel = new ChartPanel(chart);
			chartPanel.setPreferredSize(new Dimension(500, 200)); 
			setContentPane(chartPanel);
		}
		
	}
}
