package formfactors;

public class RStarAndF {

	double rStar = -1;
	double f = -1;
	
	public RStarAndF() {}

	public double getrStar() {
		return rStar;
	}

	public void setrStar(double rStar) {
		this.rStar = rStar;
	}

	public double getF() {
		return f;
	}

	public void setF(double f) {
		this.f = f;
	}

	
}
